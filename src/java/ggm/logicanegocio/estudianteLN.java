/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ggm.logicanegocio;

import ggm.accesodatos.estudianteAD;
import com.google.gson.Gson;
import ggm.accesodatos.actividadAD;
import ggm.accesodatos.docenteAD;
import ggm.comun.actividad;
import ggm.comun.clase;
import ggm.comun.docente;
import ggm.comun.estudiante;
import ggm.comun.paralelo;
import ggm.comun.parcial;
import ggm.comun.representante;
import ggm.conexion.AccesoDatos;
import java.util.ArrayList;
import java.util.List;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Logger;
//import org.json.simple.JSONArray;
//import org.json.simple.JSONObject;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author Cristhian
 */
public class estudianteLN {
    private AccesoDatos ad;
    private Gson objetoGson;
    private String strSQL;
    private estudianteAD objetoEstudianteAD;
    private estudiante objetoEstudiante;
    JSONParser oJsonParser;
    Object obj;
    JSONObject oJsonObject;
    JSONArray oJsonArray;
    
    public estudianteLN(){
        ad = new AccesoDatos();
        objetoGson = new Gson();
        strSQL = "";
        objetoEstudianteAD = new estudianteAD();
        objetoEstudiante = new estudiante();
        oJsonParser = new JSONParser();
    }
    
//MÉTODO PARA EL INGRESO DE UN ESTUDIANTE--------------------------------
    public String registrarEstudiante(String json){
        Integer result = -1;
        try{
            estudiante objetoEstudiante = objetoGson.fromJson(json, estudiante.class);
            strSQL = objetoEstudianteAD.getSQLToRegistrarEstudiante(objetoEstudiante);
            if(!strSQL.equals("")){
                if(ad.Connectar() == 2){
                    ad.BeginTran();
                    result = ad.EjecutarUpdate(strSQL);
                    ad.CommitTran();
                    ad.Desconectar();
                }
            } 
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        return result.toString();
    }
    
/*
    MÉTODO PARA EL INGRESO DE ESTUDIANTES DE UN PARALELO--------------------------
    */
    public String ingresoEstudiantesParalelo(String jsonParalelo, String listado){
        
        Integer result = -1;
        try{
            List<String> lista = listaEstudiantes(listado);                     //RETORNA EL LISTADO CONVERTIDO DEL STRING DE LA INTERFAZ
            if(ad.Connectar() == 2){
                ad.BeginTran();
                for(int i = 0; i < lista.size(); i++){
                    System.out.println("Lista aqui: "+lista.get(i));
                    if(lista.get(i).equals(";")){
                        result = estudianteLN.this.registrarEstudiante(lista.get(i-3), lista.get(i-2), lista.get(i-1), jsonParalelo);
                    }
                } 
               ad.CommitTran();
               ad.Desconectar();
            } 
            
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        return result.toString();
    }
    
    //RETORNA EL LISTADO CONVERTIDO DEL STRING DE LA INTERFAZ-------------------
    public List<String> listaEstudiantes(String strMain){
        String[] arrSplit = strMain.split("[, ]");
        List<String> lista = new ArrayList<>();
        for (int i = 0; i < arrSplit.length; i++){ 
            if(!arrSplit[i].equals("")){
                lista.add(arrSplit[i]);
            }
        }
        return lista;
    }
    
//INGRESA EL ESTUDIANTE EN LA TABLA ESTUDIANTEPARALELO Y/O LO REGISTRA----------
    public Integer registrarEstudiante(String cedula, String Apellido, String Nombre, String jsonParalelo) {
        Integer result = 0;
        try{
            System.out.println(cedula+Apellido+Nombre);

            objetoEstudiante.setStrCedula(cedula);
            objetoEstudiante.setStrApellidos(Apellido);
            objetoEstudiante.setStrNombres(Nombre);

            strSQL = objetoEstudianteAD.getSQLToBuscarIdPorCedula(objetoEstudiante);

            paralelo objetoParalelo = objetoGson.fromJson(jsonParalelo, paralelo.class);

            if(!strSQL.equals("")){
                if(consultarId(strSQL) != 0){
                    Integer aux = consultarId(strSQL);                          //RETORNA EL ID DEL ESTUDIANTE MEDIANTE SU CEDULA

                    String sqlEstudianteParalelo = objetoEstudianteAD.getSQLToRegistrarMatricula(aux, objetoParalelo);
                    if(!sqlEstudianteParalelo.equals("")){
                        result = ad.EjecutarUpdate(sqlEstudianteParalelo);
                        if(result != 2){
                            strSQL = objetoEstudianteAD.getSQLToBuscarIdUltimaMatriculaEstudiante(aux);
                            ad.EjecutarSQL(strSQL);
                            ResultSet rs = ad.getRs();
                            if(rs.next()){
                                aux = rs.getInt("idmatricula");
                            }
                            if(aux != 0){
                                strSQL = objetoEstudianteAD.getSQLToRegistrarCursaEstudiante(aux, objetoParalelo);
                                result = ad.EjecutarUpdate(strSQL);
                            } 
                        }
                    }
                }else {
                    String sqlEstudiante = objetoEstudianteAD.getSQLToInsertEstudianteDatos(objetoEstudiante);
                    result = ad.EjecutarUpdate(sqlEstudiante);

                    estudianteLN.this.registrarEstudiante(cedula, Apellido, Nombre, jsonParalelo);
                }
        }
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        return result;
    }
    
    //CONSULTAR EL ID DEL ESTUDIANTE MEDIANTE SU CEDULA-------------------------
    public Integer consultarId(String consulta){
        Integer aux = 0;
        try{    
            ad.EjecutarSQL(consulta);
            ResultSet rs = ad.getRs();
            if(rs.next()){
                aux = rs.getInt("id_estudiante");
            }
        }
        catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        return aux;
    }
    
//MOSTRAR INFORMACIÓN DE UN ESTUDIANTE--------------------------------------
    public String perfilEstudiante(String json){
        String result = "-1";
        Boolean existe = false;
        try{
            objetoEstudiante = objetoGson.fromJson(json, estudiante.class);
            strSQL = objetoEstudianteAD.getSQLToBuscarEstudiante(objetoEstudiante);
            if(!strSQL.equals("")){
                if(ad.Connectar()==2){
                    ad.EjecutarSQL(strSQL);
                    ResultSet rs = ad.getRs();
                    if(rs.next()){
                        objetoEstudiante.setIntIdEstudiante(rs.getInt("id_estudiante"));
                        objetoEstudiante.setIntIdRepresentante(rs.getInt("id"));
                        objetoEstudiante.setStrApellidos(rs.getString("apellido_est"));
                        objetoEstudiante.setStrNombres(rs.getString("nombre_est"));
                        objetoEstudiante.setStrCedula(rs.getString("cedula_est"));
                        objetoEstudiante.setIntEdad(rs.getInt("edad_est"));
                        objetoEstudiante.setStrTelefonoReferencia(rs.getString("telefonoreferencia"));
                        
                        existe = true;
                    }
                    ad.CommitTran();
                    ad.Desconectar();
                }
            }
        }
        catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        if(existe){
            result = objetoGson.toJson(objetoEstudiante);
        }
        return result;
    }
    
//MÉTODO PARA EL LISTADO DE ESTUDIANTES DE UN REPRESENTADO----------------------
    public String listadoEstudiantePorRepresentante(String json){
        String result = "-1";
        Boolean existe = false;
        List<estudiante> estudiantes = new ArrayList<>();
        try{
            representante objetoRepresentante = objetoGson.fromJson(json, representante.class);
            strSQL = objetoEstudianteAD.getSQLToListarEstudiantesDeRepresentante(objetoRepresentante);
            if(!strSQL.equals("")){
                if(ad.Connectar()==2){
                    ad.EjecutarSQL(strSQL);
                    ResultSet rs = ad.getRs();
                    while(rs.next()){
                        estudiante aux = new estudiante();
                        aux.setIntIdEstudiante(rs.getInt("id_estudiante"));
                        aux.setIntIdRepresentante(rs.getInt("id"));
                        aux.setStrApellidos(rs.getString("apellido_est"));
                        aux.setStrNombres(rs.getString("nombre_est"));
                        aux.setStrCedula(rs.getString("cedula_est"));
                        aux.setIntEdad(rs.getInt("edad_est"));
                        aux.setStrTelefonoReferencia(rs.getString("telefonoreferencia"));
                        estudiantes.add(aux);
                        existe = true;
                    }
                }
            }        
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        if(existe){
            result = objetoGson.toJson(estudiantes);
            System.out.println(result);
        }
        return result;
    }

//MÉTODO PARA MODIFICAR LA INFORMACIÓN DE UN ESTUDIANTE-------------------------
    public String modificarEstudiante(String json){
        Integer result = -1;
        try{
            objetoEstudiante = objetoGson.fromJson(json, estudiante.class);
            strSQL = objetoEstudianteAD.getSQLToModificarEstudiante(objetoEstudiante);
            if(!strSQL.equals("")){
                if(ad.Connectar() == 2){
                    ad.BeginTran();
                    result = ad.EjecutarUpdate(strSQL);
                    ad.CommitTran();
                    ad.Desconectar();
                }
            }
            
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        return result.toString();
    }

//LÓGICA PARA EL LISTADO DE ESTUDIANTES DE UN PARALELO--------------------------
    public String listarEstudiantes(String jsonParalelo) {
        
        String result = "-1";
        Boolean existe = false;
        List<estudiante> estudiantes = new ArrayList<>();
        try{
            paralelo objetoParalelo = objetoGson.fromJson(jsonParalelo, paralelo.class);
            strSQL = objetoEstudianteAD.getSQLToListarEstudiantesDeParalelo(objetoParalelo);
            if(!strSQL.equals("")){
                if(ad.Connectar()==2){
                    ad.EjecutarSQL(strSQL);
                    ResultSet rs = ad.getRs();
                    while(rs.next()){
                        estudiante aux = new estudiante();
                        aux.setIntIdEstudiante(rs.getInt("id_estudiante"));
                        aux.setIntIdRepresentante(rs.getInt("id"));
                        aux.setStrApellidos(rs.getString("apellido_est"));
                        aux.setStrNombres(rs.getString("nombre_est"));
                        aux.setStrCedula(rs.getString("cedula_est"));
//                        aux.setIntEdad(rs.getInt("edad_est"));
//                        aux.setStrTelefonoReferencia(rs.getString("telefonoreferencia"));
                        estudiantes.add(aux);
                        existe = true;
                    }
                }
            }        
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        if(existe){
            result = objetoGson.toJson(estudiantes);
            System.out.println(result);
        }
        return result;   
    }
    
//LÓGICA PARA EL LISTADO DE DOCENTES DE UN ESTUDIANTE---------------------------
    public String listarDocentesEstudiante(String jsonEstudiante) {
        String result = "-1";
        Boolean existe = false;
        List<docente> docentes = new ArrayList<>();
        try{
            estudiante objetoEstudiante = objetoGson.fromJson(jsonEstudiante, estudiante.class);
            docenteAD objetoDocenteAD = new docenteAD();
            strSQL = objetoDocenteAD.getSQLToListarDocentesDeEstudiante(objetoEstudiante);
            if(!strSQL.equals("")){
                if(ad.Connectar()==2){
                    ad.EjecutarSQL(strSQL);
                    ResultSet rs = ad.getRs();
                    while(rs.next()){
                        docente aux = new docente();
                        aux.setIntIdUsuario(rs.getInt("id"));
                        aux.setStrNombre(rs.getString("nombres"));
                        aux.setStrApellido(rs.getString("apellidos"));
                        aux.setStrCedula(rs.getString("cedula"));
                        aux.setStrTitulo1(rs.getString("nombre_mat"));
                        
                        docentes.add(aux);
                        existe = true;
                    }
                }
            }        
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        if(existe){
            result = objetoGson.toJson(docentes);
            System.out.println(result);
        }
        return result; 
    }
    
//LÓGICA PARA EL LISTADO DE ESTUDIANTES SIN REPRESENTANTE ASIGNADO--------------
    public String listarEstudiantesSinRepresentante(String jsonParalelo) {
        String result = "-1";
        Boolean existe = false;
        List<estudiante> estudiantes = new ArrayList<>();
        try{
            paralelo objetoParalelo = objetoGson.fromJson(jsonParalelo, paralelo.class);
            estudianteAD objetoDocenteAD = new estudianteAD();
            strSQL = objetoDocenteAD.getSQLToListarEstudiantesSinRepresentante(objetoParalelo);
            
            if(!strSQL.equals("")){
                if(ad.Connectar()==2){
                    ad.EjecutarSQL(strSQL);
                    ResultSet rs = ad.getRs();
                    while(rs.next()){
                        estudiante aux = new estudiante();
                        aux.setIntIdEstudiante(rs.getInt("id_estudiante"));
                        aux.setStrNombres(rs.getString("nombre_est"));
                        aux.setStrApellidos(rs.getString("apellido_est"));
                        aux.setStrCedula(rs.getString("cedula_est"));
                        estudiantes.add(aux);
                        existe = true;
                    }
                }
            }        
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        if(existe){
            result = objetoGson.toJson(estudiantes);
            System.out.println(result);
        }
        return result; 
    }
    
//LÓGICA PARA ASIGNAR UN REPRESENTANTE A UN ESTUDIANTE--------------------------    
    public String asignarRepresentanteEstudiante(String jsonRepresentante, String jsonEstudiante) {
        Integer result = -1;
        try{
            representante objetoRepresentante = objetoGson.fromJson(jsonRepresentante, representante.class);
            estudiante objetoEstudiante = objetoGson.fromJson(jsonEstudiante, estudiante.class);
            strSQL = objetoEstudianteAD.getSQLToAsignarRepresentanteEstudiante(objetoRepresentante, objetoEstudiante);
            
            if(!strSQL.equals("")){
                if(ad.Connectar() == 2){
                    ad.BeginTran();
                    result = ad.EjecutarUpdate(strSQL);
                    ad.CommitTran();
                    ad.Desconectar();
                }
            }
            
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        return result.toString();
    }
    
//LÓGICA PARA EL LISTADO DE ACTIVIDADES Y SUS NOTAS DE UN ESTUDIANTE------------
    public String listarActividadesNotasEstudiantes(String jsonEstudiante) {
        String result = "-1";
        Boolean existe = false;
        List<actividad> actividades = new ArrayList<>();
        //JSONArray lista = new JSONArray();
        try{
            estudiante objetoEstudiante = objetoGson.fromJson(jsonEstudiante, estudiante.class);
            actividadAD objetoActividadAD = new actividadAD();
            
            strSQL = objetoActividadAD.getSQLToListarActividadesNotasEstudiante(objetoEstudiante);
            
            if(!strSQL.equals("")){
                if(ad.Connectar()==2){
                    ad.EjecutarSQL(strSQL);
                    ResultSet rs = ad.getRs();
                    while(rs.next()){
                        actividad aux = new actividad();
                        aux.setIntIdActividad(rs.getInt("id_actividad"));
                        aux.setStrNombre(rs.getString("nombre_act"));
                        aux.setStrFechaRealizado(rs.getString("f_realizado"));
                        aux.setStrFechaPlazo(rs.getString("f_plazo"));
                        aux.setStrDescripcion(rs.getString("descripcion_act"));
                        aux.setStrTipo(rs.getString("tipo_act"));
                        
                        aux.setIntIdClase(rs.getInt("calificacion"));
                        actividades.add(aux);
                        existe = true;
                    }
                }
            }        
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        if(existe){
            result = objetoGson.toJson(actividades);
            System.out.println(result);
        }
        return result; 
    }    
    
    
//LÓGICA PARA EL LISTADO DE ESTUDIANTES DE UNA CLASE Y UNA ACTIVIDAD------------
    public String listarEstudiantesClaseActividad(String jsonActividad) {
        String result = "-1";
        Boolean existe = false;
        List<estudiante> lista = new ArrayList<>();
        
        //JSONArray lista = new JSONArray();
        try{
            //clase objetoClase = objetoGson.fromJson(jsonClase, clase.class);
            actividad objetoActividad = objetoGson.fromJson(jsonActividad, actividad.class);
            strSQL = objetoEstudianteAD.getSQLToListarEstudiantesClaseActividadNotas(objetoActividad);
            
            if(!strSQL.equals("")){
                if(ad.Connectar()==2){
                    ad.EjecutarSQL(strSQL);
                    ResultSet rs = ad.getRs();
                    while(rs.next()){
                        
                            estudiante objetoEstudiante = new estudiante();
                            objetoEstudiante.setStrNombres(rs.getString("nombre_est"));
                            objetoEstudiante.setStrApellidos(rs.getString("apellido_est"));
                            objetoEstudiante.setStrCedula(rs.getString("cedula_est"));
                            objetoEstudiante.setIntEdad(rs.getInt("calificacion"));
                            
                        lista.add(objetoEstudiante);
                        existe = true;
                    }
//                    ad.CommitTran();
                    ad.Desconectar();
                }
            }        
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        if(existe){
            result = objetoGson.toJson(lista);
            System.out.println(result);
        }
        return result; 
    }     
    
    
    
    
    
    
    
}
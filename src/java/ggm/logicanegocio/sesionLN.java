/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ggm.logicanegocio;

import ggm.accesodatos.sesionAD;
import com.google.gson.Gson;
import ggm.accesodatos.administrativoAD;
import ggm.accesodatos.periodoacademicoAD;
import ggm.accesodatos.usuarioAD;
import ggm.comun.administrativo;
import ggm.comun.docente;
import ggm.comun.paralelo;
import ggm.comun.periodo;
import ggm.comun.representante;
import ggm.comun.usuario;

import ggm.conexion.AccesoDatos;
import ggm.accesodatos.docenteAD;
import ggm.accesodatos.representanteAD;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author Cristhian
 */
public class sesionLN {
    private AccesoDatos ad;
    private sesionAD objetoSesionAD;
    private Gson objetoGson;
    private String strSQL;
    private usuario objetoUsuario;
    private Integer idperiodo = 0;
    private Integer idusuario = 0;
    periodo oPeriodo;
    
    public sesionLN(){
        ad = new AccesoDatos();
        objetoSesionAD = new sesionAD();
        objetoGson = new Gson();
        strSQL = "";
        objetoUsuario = new usuario();
        oPeriodo = new periodo();
    }
    
//MÉTODO PARA EL LOGEO DE USUARIOS---------------------------------------------  
    public String loginUsuarios(String json){
        String result = "-1";
        Boolean existe = false;
        try{
            strSQL = objetoSesionAD.getSQLToLogin(json);
            if(!strSQL.equals("")){
                if(ad.Connectar() == 2){
                    ad.BeginTran();
                    ad.EjecutarSQL(strSQL);
                    ResultSet rs = ad.getRs();
                    ad.CommitTran();
                    ad.Desconectar();
                    if (rs.next()) {
                        objetoUsuario.setIntIdUsuario(rs.getInt("id"));
                        objetoUsuario.setStrCedula(rs.getString("cedula"));
                        objetoUsuario.setStrNombre(rs.getString("nombres"));
                        objetoUsuario.setStrApellido(rs.getString("apellidos"));
                        objetoUsuario.setStrTipo(rs.getString("tipo"));
                        objetoUsuario.setIntPeriodo(rs.getInt("id_periodo"));
                        existe = true;
                    }
                }
            }
            
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        if (existe) {
            result = objetoGson.toJson(objetoUsuario);
        }
        return result;
    }
    
//MÉTODO PARA EL INGRESO DE USUARIOS--------------------------------------------
public String registroUsuario(String json, String jsonPeriodo){
        String resultado = "-1";
        try{
            
            if(ad.Connectar() == 2){
                periodo objetoPeriodo = objetoGson.fromJson(jsonPeriodo, periodo.class);
                periodoacademicoAD objetoPeriodoAD = new periodoacademicoAD();
                ad.BeginTran();
                System.out.println("ID PERIODO "+objetoPeriodo.getIntIdPeriodo());
                if(objetoPeriodo.getIntIdPeriodo() != 0){
                    idperiodo = objetoPeriodo.getIntIdPeriodo();
                    System.out.println("Periodo ya creado");
                }else{
                    strSQL = objetoPeriodoAD.getSQLToIngresarPeriodo(objetoPeriodo);
                    ad.EjecutarUpdate(strSQL);
                    strSQL = objetoPeriodoAD.getSQLToBuscarUltimoPeriodo();
                    ad.EjecutarSQL(strSQL);
                    ResultSet rs = ad.getRs();
                    if(rs.next()){
                        idperiodo = rs.getInt("id_periodo");
                        System.out.println("Periodo nuevo");
                    }else {
                        resultado = "Ocurrio un problema al registrar el período académico";
                    }
                }
                objetoUsuario = objetoGson.fromJson(json, usuario.class);
                objetoUsuario.setIntPeriodo(idperiodo);
                usuarioAD objetoUsuarioAD = new usuarioAD();
                strSQL = objetoUsuarioAD.getSQLToIngresarUsuario(objetoUsuario);
                if(!strSQL.equals("")){
                    resultado = ad.EjecutarUpdate(strSQL).toString();
                    System.out.println("Usuario respuesta: "+resultado);
                    if(!resultado.equals("-2")){
                        strSQL = objetoUsuarioAD.getSQLToBuscarIdUsuario(objetoUsuario);
                        ad.EjecutarSQL(strSQL);
                        ResultSet rs = ad.getRs();
                        if(rs.next()){
                            idusuario = rs.getInt("id");
                        }
                        switch(objetoUsuario.getStrTipo()){
                        case "Representante":
                            representante objetoRepresentante = objetoGson.fromJson(json, representante.class);
                            representanteAD objetoRepresentanteAD = new representanteAD();
                            objetoRepresentante.setIntIdUsuario(idusuario);
                            objetoRepresentante.setIntPeriodo(idperiodo);
                            strSQL = objetoRepresentanteAD.getSQLToIngresarRepresentante(objetoRepresentante);
                            ad.EjecutarUpdate(strSQL);
                            resultado = "Representante registrado con éxito";
                            System.out.println(1+" "+strSQL);
                            break;
                        case "Docente":
                            docente objetoDocente = objetoGson.fromJson(json, docente.class);
                            docenteAD objetoDocenteAD = new docenteAD();
                            objetoDocente.setIntIdUsuario(idusuario);
                            objetoDocente.setIntPeriodo(idperiodo);
                            strSQL = objetoDocenteAD.getSQLToInsertarDocente(objetoDocente);
                            ad.EjecutarUpdate(strSQL);
                            resultado = "Docente registrado con éxito";
                            System.out.println(2+" "+strSQL);
                            break;
                        case "Administrativo":
                            administrativo objetoAdministrativo = objetoGson.fromJson(json, administrativo.class);
                            administrativoAD objetoAdministrativoAD = new administrativoAD();
                            objetoAdministrativo.setIntIdUsuario(idusuario);
                            objetoAdministrativo.setIntPeriodo(idperiodo);
                            strSQL = objetoAdministrativoAD.getSQLToIngresarAdministrativo(objetoAdministrativo);
                            ad.EjecutarUpdate(strSQL);
                            resultado = "Administrativo registrado con éxito";
                            System.out.println(3+" "+strSQL);
                           break;
                        } 
                    } 
                }
                //objetoSesionAD.SQLToRegistrarTipo(json, idperiodo, ad);
                //strSQL = objetoSesionAD.getSQLToRegistrar(json, idperiodo);
                ad.CommitTran();
                ad.Desconectar();
            }else{resultado = "No se pudo conectar a la base de datos";}
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        return resultado.toString();
    }  

//MÉTODO PARA EL INGRESO O SUBIDA DE TOKEN--------------------------------------
public String subirToken(String json) {
         Integer result = -1;
         String tipo = "";
         try{
                JSONParser parser = new JSONParser();
                Object obj = parser.parse(json);
                JSONObject oJson = (JSONObject) obj;
                String strCedula = (String)oJson.get("strCedula");
                String strToken = (String)oJson.get("strToken");
                usuario objetoUsuario = new usuario();
                objetoUsuario.setStrCedula(strCedula);
                objetoUsuario.setStrToken(strToken);
                usuarioAD objetoUsuarioAD = new usuarioAD();
                strSQL = objetoUsuarioAD.getSQLToAsignarTokenUsuario(objetoUsuario);
               
             if(!strSQL.equals("")){
                 if(ad.Connectar() == 2){
                     ad.BeginTran();
                    result = ad.EjecutarUpdate(strSQL);
                    if(result == 1){
                        strSQL = objetoUsuarioAD.getSQLToTipoUsuario(objetoUsuario);
                        ad.EjecutarSQL(strSQL);
                        ResultSet rs = ad.getRs();
                        if(rs.next()){
                            tipo = rs.getString("tipo");
                        }
                        if(!tipo.equals("")){//Segun el tipo de usuario se actualiza en su tabla correspondiente
                            switch(tipo){
                                case "Representante":
                                    representante objetoRepresentante = new representante();
                                    objetoRepresentante.setStrCedula(strCedula);
                                    objetoRepresentante.setStrToken(strToken);
                                    representanteAD oRepresentanteAD = new representanteAD();
                                    strSQL = oRepresentanteAD.getSQLToAsignarTokenRepresentante(objetoRepresentante);
                                    break;
                                case "Docente":
                                    docente objetoDocente = new docente();
                                    objetoDocente.setStrCedula(strCedula);
                                    objetoDocente.setStrToken(strToken);
                                    docenteAD objetoDocenteAD = new docenteAD();
                                    strSQL = objetoDocenteAD.getSQLToAsignarTokenDocente(objetoDocente);
                                    break;
                                default : result = -1;
                            }
                            if(result != -1){
                                result = ad.EjecutarUpdate(strSQL);
                            }
                        }
                    }
                    ad.CommitTran();
                    ad.Desconectar();
                 }
             }
         }catch(Exception e){
             Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
         }
        return result.toString();
    }
   
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ggm.logicanegocio;

import ggm.accesodatos.actividadAD;
import com.google.gson.Gson;
import ggm.accesodatos.claseAD;
import ggm.accesodatos.docenteAD;
import ggm.accesodatos.estudianteAD;
import ggm.accesodatos.materiaAD;
import ggm.accesodatos.notificacionAD;
import ggm.accesodatos.paraleloAD;
import ggm.accesodatos.representanteAD;
import ggm.comun.actividad;
import ggm.comun.calificacion;
import ggm.comun.clase;
import ggm.comun.docente;
import ggm.comun.estudiante;
import ggm.comun.materia;
import ggm.comun.notificacion;
import ggm.comun.paralelo;
import ggm.comun.representante;
import ggm.conexion.AccesoDatos;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

//import org.json.JSONArray;
//import org.json.JSONObject;

/**
 *
 * @author Cristhian
 */
public class actividadLN {
    private AccesoDatos ad;
    private actividadAD objetoActividadAD;
    private Gson objetoGson;
    private String strSQL;
    private actividad objetoActividad;
    
    public actividadLN(){
        ad = new AccesoDatos();
        objetoActividadAD = new actividadAD();
        objetoGson = new Gson();
        strSQL = "";
        objetoActividad = new actividad();
    }

    
    /*
    *En la parte de convergencia
    */
    String listarActividadesFechaU(String json) {
        
        return "";
    }

//MÉTODO PARA LA MODIFICACION DE UNA ACTIVIDAD ACADÉMICA------------------------    
    public String modificarActividad(String json) {
        Integer result = -1;
        try{
            objetoActividad = objetoGson.fromJson(json, actividad.class);
            strSQL = objetoActividadAD.getSQLToModificarActividad(objetoActividad);
            if(!strSQL.equals("")){
                if(ad.Connectar() == 2){
                    ad.BeginTran();
                    result = ad.EjecutarUpdate(strSQL);
                    ad.CommitTran();
                    ad.Desconectar();
                }
            }
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        return result.toString();
    }
    
//MÉTODO PARA CREAR OBSERVACIONES DE UNA ACTIVIDAD------------------------------
    public String responderActividad(String jsonNotificacion, String jsonActividad, String jsonDocente, String jsonRepresentante, String jsonParalelo, String jsonMateria, String jsonEstudiante, String jsonCalificacion){
        Integer result = -1;
        try{
            String token = "";
            String jsonActividadN = "";
            notificacion objetoNotificacion = objetoGson.fromJson(jsonNotificacion, notificacion.class);
            calificacion objetoCalificacion = objetoGson.fromJson(jsonCalificacion, calificacion.class);
            objetoActividad = objetoGson.fromJson(jsonActividad, actividad.class);
            
            representante objetoRepresentante = objetoGson.fromJson(jsonRepresentante, representante.class);
            docente objetoDocente = objetoGson.fromJson(jsonDocente, docente.class);
            
            notificacionAD objetoNotificacionAD = new notificacionAD();
            
            if(ad.Connectar() == 2){
                ad.BeginTran();
                
                
                if(objetoNotificacion.getBooEstadoDocente() == true && objetoNotificacion.getBooEstadoRepresentante() == false){
                    token = objetoDocente.getStrToken();
                    objetoNotificacion.setStrNombreNotificacion(objetoRepresentante.getStrNombre()+" ha realizado una observacion a "+objetoActividad.getStrNombre());
                }else if(objetoNotificacion.getBooEstadoRepresentante() == true && objetoNotificacion.getBooEstadoDocente() == false){
                    token = objetoRepresentante.getStrToken();
                    objetoNotificacion.setStrNombreNotificacion(objetoDocente.getStrNombre()+" ha realizado una observacion a "+objetoActividad.getStrNombre());
                }
                
                strSQL = objetoNotificacionAD.getSQLToModificarNotificacion(objetoNotificacion);
                result = ad.EjecutarUpdate(strSQL);
                
                strSQL = objetoActividadAD.getSQLToModificarCalificacionActividad(objetoCalificacion);
                result = ad.EjecutarUpdate(strSQL);
                
                strSQL = objetoActividadAD.getSQLToBuscarActividad(objetoActividad);
                ad.EjecutarSQL(strSQL);
                ResultSet rs = ad.getRs();
                if(rs.next()){
                    objetoActividad.setIntIdActividad(rs.getInt("id_actividad"));
                    objetoActividad.setIntIdClase(rs.getInt("idclase"));
                    objetoActividad.setStrNombre(rs.getString("nombre_act"));
                    objetoActividad.setStrFechaRealizado(rs.getString("f_realizado"));
                    objetoActividad.setStrFechaPlazo(rs.getString("f_plazo"));
                    objetoActividad.setStrDescripcion(rs.getString("descripcion_act"));
                    objetoActividad.setStrTipo(rs.getString("tipo_act"));
                    jsonActividadN = objetoGson.toJson(objetoActividad);
                }else jsonActividadN =jsonActividad;
                
                enviarNotificacion(token, objetoNotificacion, jsonActividadN, jsonDocente, jsonRepresentante, jsonParalelo, jsonMateria, jsonEstudiante, jsonCalificacion);
                ad.CommitTran();
                ad.Desconectar();
            }
            
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        return result.toString();
    }
        
//MÉTODO PARA EL INGRESO DE UNA ACTIVIDAD POR PARTE DEL DOCENTE----------------- 
    public String ingresarActividad(String jsonClase, String jsonActividad, String lista) {
        System.out.println("VALOR DEL JSON CLASE "+jsonClase);
        Integer result = -1;
        Integer IdUltimaActividadIngresada = 0;
        Integer auxIdEstudiante = 0;
        Integer auxIdRepresentante = 0;
        String strToken = "";
        String notificacion = "";
        
        String jsonDocente = "";
        String jsonParalelo = "";
        String jsonMateria = "";
        String jsonEstudiante = "";
        String jsonCalificacion = "";
        String jsonRepresentante = "";
        
        docente objetoDocente = new docente();
        docenteAD objetoDocenteAD = new docenteAD();
        paralelo objetoParalelo = new paralelo();
        paraleloAD objetoParaleloAD = new paraleloAD();
        materia objetoMateria = new materia();
        materiaAD objetoMateriaAD = new materiaAD();
        notificacion objetoNotificacion = new notificacion();
        estudiante objetoEstudiante = new estudiante();
        estudianteAD objetoEstudianteAD = new estudianteAD();
        calificacion objetoCalificacion = new calificacion();
        
        representanteAD objetoRepresentanteAD = new representanteAD();
        representante objetoRepresentante = new representante();
        
        Date objetoDate = new Date();
        try{
            claseAD objetoClaseAD = new claseAD();
            clase objetoClase = objetoGson.fromJson(jsonClase, clase.class);
            strSQL = objetoClaseAD.getSQLToBuscarIdClase(objetoClase);
            objetoActividad = objetoGson.fromJson(jsonActividad, actividad.class);
            if(ad.Connectar() == 2){
                ad.BeginTran();
                ad.EjecutarSQL(strSQL);
                ResultSet rs = ad.getRs();
                if(rs.next()){
                    objetoActividad.setIntIdClase(rs.getInt("idclase"));
                }
                
                strSQL = objetoDocenteAD.getSQLToBuscarDocenteForClase(objetoClase);
                ad.EjecutarSQL(strSQL);
                rs = ad.getRs();
                if(rs.next()){
                    objetoDocente.setIntIdUsuario(rs.getInt("id"));
                    objetoDocente.setStrNombre(rs.getString("nombres"));
                    objetoDocente.setStrApellido(rs.getString("apellidos"));
                    objetoDocente.setStrToken(rs.getString("tokenmovil"));
                    jsonDocente = objetoGson.toJson(objetoDocente);
                }
                
                strSQL = objetoParaleloAD.getSQLToBuscarParaleloForClase(objetoClase);
                ad.EjecutarSQL(strSQL);
                rs = ad.getRs();
                if(rs.next()){
                    objetoParalelo.setIntIdParalelo(rs.getInt("id_paralelo"));
                    objetoParalelo.setStrParalelo(rs.getString("paralelo"));
                    jsonParalelo = objetoGson.toJson(objetoParalelo);
                }
                
                strSQL = objetoMateriaAD.getSQLToBuscarMateriaForClase(objetoClase);
                ad.EjecutarSQL(strSQL);
                rs = ad.getRs();
                if(rs.next()){
                    objetoMateria.setIntIdMAteria(rs.getInt("id_materia"));
                    objetoMateria.setStrNombreMAteria(rs.getString("nombre_mat"));
                    jsonMateria = objetoGson.toJson(objetoMateria);
                }
                
                
            }
            strSQL = objetoActividadAD.getSQLToIngresarActividad(objetoActividad);
            result = ad.EjecutarUpdate(strSQL);
            strSQL = objetoActividadAD.getSQLToSelectLastIdActivity();
            ad.EjecutarSQL(strSQL);
            ResultSet rs = ad.getRs();
            if(rs.next()){
                IdUltimaActividadIngresada = rs.getInt("id_actividad");
                objetoActividad.setIntIdActividad(IdUltimaActividadIngresada);
                jsonActividad = objetoGson.toJson(objetoActividad);
            }
            System.out.println("id Actividad: "+IdUltimaActividadIngresada);
            
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(lista);
            JSONObject jsonObject = (JSONObject) obj;
            JSONArray msg = (JSONArray) jsonObject.get("id");
            
            for (Object object : msg) { //Recuperamos la consulta por cada estudiante que ha sido enviada la tarea 
                auxIdEstudiante = Integer.parseInt(object.toString());
                objetoEstudiante.setIntIdEstudiante(auxIdEstudiante);
                strSQL = objetoActividadAD.getSQLToIngresarEstudianteActividad(IdUltimaActividadIngresada, auxIdEstudiante);
                objetoCalificacion.setIntIdActividad(IdUltimaActividadIngresada);
                objetoCalificacion.setIntIdEstudiante(auxIdEstudiante);
                objetoCalificacion.setIntCalificacion(0);
                jsonCalificacion =objetoGson.toJson(objetoCalificacion);
                
                result = ad.EjecutarUpdate(strSQL);
                System.out.println("Resultado EstudianteActividad: "+result);
                
                if(result != -1){
                     
                    strSQL = objetoEstudianteAD.getSQLToBuscarRepresentanteDeEstudiante(objetoEstudiante);
                    
                    ad.EjecutarSQL(strSQL);
                    rs = ad.getRs();
                    if(rs.next()){
                        strToken = rs.getString("tokenmovil");
                        auxIdRepresentante = rs.getInt("id");
                        objetoRepresentante.setIntIdUsuario(auxIdRepresentante);
                        objetoRepresentante.setStrNombre(rs.getString("nombres"));
                        objetoRepresentante.setStrApellido(rs.getString("apellidos"));
                        objetoRepresentante.setStrToken(rs.getString("tokenmovil"));
                        jsonRepresentante = objetoGson.toJson(objetoRepresentante);
                        
                        objetoEstudiante.setStrNombres(rs.getString("nombre_est"));
                        objetoEstudiante.setStrApellidos(rs.getString("apellido_est"));
                        jsonEstudiante = objetoGson.toJson(objetoEstudiante);
                        
                        System.out.println("Consulta token: "+strToken);
                    }
                    if(strToken != "" && auxIdRepresentante != 0){
                        objetoNotificacion.setIntIdDocente(objetoClase.getIntIdDocente());
                        objetoNotificacion.setIntIdRepresentante(auxIdRepresentante);
                        objetoNotificacion.setBooEstadoDocente(false);
                        objetoNotificacion.setBooEstadoRepresentante(true);
                        objetoNotificacion.setStrNombreNotificacion(objetoDocente.getStrNombre()+" "+objetoDocente.getStrApellido()+" ha creado un/a nuevo/a "+objetoActividad.getStrTipo());
                        objetoNotificacion.setIntCodigoActividad(IdUltimaActividadIngresada);
                        objetoNotificacion.setStrFechaEnvio(new Timestamp(objetoDate.getTime()).toString());
                        
                        notificacionAD objetoNotificacionAD = new notificacionAD();
                        strSQL = objetoNotificacionAD.getSQLToIngresarNotificacion(objetoNotificacion);
                        result = ad.EjecutarUpdate(strSQL);
                        
                        if(result != 2){
                            notificacion = enviarNotificacion(strToken, objetoNotificacion, jsonActividad, jsonDocente, jsonRepresentante, jsonParalelo, jsonMateria, jsonEstudiante, jsonCalificacion);
                        }
                    }
                }   
            }
            ad.CommitTran();
            ad.Desconectar();
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        return result.toString()+notificacion;
    }
        
//MÉTODO PARA ENVIAR UNA NOTIFICACIÓN-------------------------------------------    
    public String enviarNotificacion (String strToken, notificacion objetoNotificacion, String jsonActividad, String jsonDocente, String jsonRepresentante, String jsonParalelo, String jsonMateria, String jsonEstudiante, String jsonCalificacion){
        String respuesta = "";
        Gson objetoGson = new Gson();
        try{
            String jsonNotificacion = objetoGson.toJson(objetoNotificacion);
            String clave = "AAAApuEgb48:APA91bFABROD_opPlFslMYjZy1xBMYmaWKMlzMQI1WFr94v6F04NCxL8VPzfL9QsNsBJgzvWwuo5cttPenc_ZCsraC5LEzNbzG0IfACc6ygB2M486u9_Ks0NkwCwIDqkrzyoQz08aS1B";
            URL url = new URL("https://fcm.googleapis.com/fcm/send");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Authorization", "key="+clave);
            //A objeto JSON------------------------------------------------------------
            JSONObject todo = new JSONObject();
            //todo.put("to", "cEhqjAq4iRY:APA91bFnJGwfOVgw6Bu9Hy7KF6tsGn2wUg_-z2BR4HPO-u2-vLE-1TrxvG7pTg8b-aA7M19sI7BxB4uVgZrenFmt_RaBp05QvSN1jTGU7nkUOleygDjcj-qpH4r471VK_ccZOWHh-sgp");
            todo.put("to", strToken);
            JSONObject data = new JSONObject();
                data.put("notificacion", jsonNotificacion);
                data.put("actividad", jsonActividad);
                data.put("docente", jsonDocente);
                data.put("representante", jsonRepresentante);
                data.put("paralelo", jsonParalelo);
                data.put("materia", jsonMateria);
                data.put("estudiante", jsonEstudiante);
                data.put("calificacion", jsonCalificacion);
            todo.put("data", data);
            //-------------------------------------------------------------------------
            String input = todo.toString();
            System.out.println("DATOS QUE SE VAN: "+input);
            OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
            wr.write(input);//escribimos
            wr.close();//cerramos la conexion
            BufferedReader rd = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String linea;
            while ((linea = rd.readLine()) != null) {
                respuesta+= linea; //procesamos la salida
                
            }
            System.out.println(respuesta);
            return respuesta;
        }
        catch(IOException e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
            System.out.println(e+" "+respuesta);
            return "Error: "+e+" "+respuesta;
        } 
    }

    //MÉTODO PARA OBTENER LOS DESTINATARIOS DE UNA ACTIVIDAD------------------------    
    public String destinatariosActividades(Integer idActividad) {
        String result = "";
        List <String> lista = new ArrayList<>();
        try{
            strSQL = objetoActividadAD.getSQLToSelectDestinatariosActividades(idActividad);
            ad.EjecutarSQL(strSQL);
            ResultSet rs = ad.getRs();
            while(rs.next()){
                lista.add(rs.getString("paralelo"));
            }
            System.out.println(lista);
            result = objetoGson.toJson(lista);
            System.out.println(result);
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        return result;
    }
    
    
    
//LÓGICA DE NEGOCIO PARA LISTAR LAS ACTIVIDADES DE LOS HIJOS DE UN REPRESENTANTE
//    public String actividadesEstudiantesDeRepresentante(String jsonRepresentante){
//        String result = "-1";
//        Boolean existe = false;
//        actividad objetoActividad = new actividad();
//        
//        JSONArray todo = new JSONArray();
//        try{
//            representante objetoRepresentante = objetoGson.fromJson(jsonRepresentante, representante.class);
//            strSQL = objetoActividadAD.getSQLToListarActividadesEstudiantesRepresentante(objetoRepresentante);
//            if(ad.Connectar() == 2){
//                ad.BeginTran();
//                ad.EjecutarSQL(strSQL);
//                ResultSet rs = ad.getRs();
//                while(rs.next()){
//                    JSONObject data = new JSONObject();
//                    data.put("estudiante", rs.getString("nombre_est"));
//                    
//                    objetoActividad.setIntIdActividad(rs.getInt("id_actividad"));
//                    objetoActividad.setIntIdClase(rs.getInt("idclase"));
//                    objetoActividad.setStrNombre(rs.getString("nombre_act"));
//                    objetoActividad.setStrFechaRealizado(rs.getString("f_realizado"));
//                    objetoActividad.setStrFechaPlazo(rs.getString("f_plazo"));
//                    objetoActividad.setStrDescripcion(rs.getString("descripcion_act"));
//                    objetoActividad.setStrTipo(rs.getString("tipo_act"));
//                    String auxActividad = objetoGson.toJson(objetoActividad);
//                    System.out.println("Actividad: "+auxActividad);
//                    data.put("actividad", auxActividad);
//                    
//                    data.put("calificacion", rs.getInt("calificacion"));
//                    todo.put(data);
//                    existe = true;
//                }
//                System.out.println("RESPUESTA: "+todo.toString());
//                
//                ad.CommitTran();
//                ad.Desconectar();
//            }
//        }catch(Exception e){
//            Logger log = Logger.getLogger(this.getClass().getName());
//            log.severe(e.getMessage());
//            ad.RollbackTran();
//        }
//        if(existe){
//            result = todo.toString();
//        }
//        return result;
//    }
    
    
    public void prueba(String p){
        String posi;
        Integer prueba = 0;
    }
    
//LÓGICA PARA EL LISTADO DE ACTIVIDADES DE UNA CLASE----------------------------
     public String listarActividadesClase(String jsonClase){
        String result = "-1";
        Boolean existe = false;
        List<actividad> actividades = new ArrayList<>();
        try{
            clase objetoClase = objetoGson.fromJson(jsonClase, clase.class);
            strSQL = objetoActividadAD.getSQLToListarActividadesClase(objetoClase);
            
            if(!strSQL.equals("")){
                if(ad.Connectar()==2){
                    ad.EjecutarSQL(strSQL);
                    ResultSet rs = ad.getRs();
                    System.out.println("Estamos aqui");
                    while(rs.next()){
                        actividad aux = new actividad();
                        aux.setIntIdActividad(rs.getInt("id_actividad"));
                        aux.setIntIdClase(rs.getInt("idclase"));
                        aux.setStrNombre(rs.getString("nombre_act"));
                        aux.setStrFechaRealizado(rs.getString("f_realizado"));
                        aux.setStrFechaPlazo(rs.getString("f_plazo"));
                        aux.setStrDescripcion(rs.getString("descripcion_act"));
                        aux.setStrTipo(rs.getString("tipo_act"));
                        
                        actividades.add(aux);
                        existe = true;
                    }
                    ad.Desconectar();
                }
            }
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        if(existe){
            result = objetoGson.toJson(actividades);
            System.out.println(result);
        }
        return result;
    }
    
}

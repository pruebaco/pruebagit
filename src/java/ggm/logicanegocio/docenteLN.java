/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ggm.logicanegocio;

import com.google.gson.Gson;
import ggm.accesodatos.materiaAD;
import ggm.accesodatos.usuarioAD;
import ggm.accesodatos.actividadAD;
import ggm.comun.docente;
import ggm.comun.actividad;
import ggm.comun.materia;
import ggm.comun.paralelo;
import ggm.comun.usuario;
//import ggm.clases.representante;
import ggm.conexion.AccesoDatos;
import ggm.accesodatos.docenteAD;
import ggm.accesodatos.notificacionAD;
import ggm.accesodatos.paraleloAD;
import ggm.comun.clase;
import ggm.comun.notificacion;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
//import ggm.representante.representanteAD;

/**
 *
 * @author Cristhian
 */
public class docenteLN {
    private AccesoDatos ad;
    private docenteAD objetoDocenteAD;
    private Gson objetoGson;
    private paralelo oParalelo;
    private String strSQL;
    private docente objetoDocente;
    
    public docenteLN(){
        ad = new AccesoDatos();
        objetoDocenteAD = new docenteAD();
        objetoGson = new Gson();
        strSQL = "";
        objetoDocente = new docente();
        oParalelo = new paralelo();
    }

//MOSTRAR INFORMACION DE UN DOCENTE---------------------------------------------
    public String perfilDocente(String json){
       String result = "-1";
        Boolean existe = false;
        try{
            docente auxDocente = objetoGson.fromJson(json, docente.class);
            strSQL = objetoDocenteAD.getSQLToBuscarDocente(auxDocente);
            if(!strSQL.equals("")){
                if(ad.Connectar() == 2){
                    ad.EjecutarSQL(strSQL);
                    ResultSet rs = ad.getRs();
                    if(rs.next()){
                        objetoDocente.setIntIdUsuario(rs.getInt("id"));
                        objetoDocente.setIntPeriodo(rs.getInt("id_periodo"));
                        objetoDocente.setStrNombre(rs.getString("nombres"));
                        objetoDocente.setStrApellido(rs.getString("apellidos"));
                        objetoDocente.setStrCedula(rs.getString("cedula"));
                        objetoDocente.setIntEdad(rs.getInt("edad"));
                        objetoDocente.setStrCorreo(rs.getString("correo"));
                        objetoDocente.setStrTelefono(rs.getString("telefono"));
                        objetoDocente.setStrCelular1(rs.getString("celular1"));
                        objetoDocente.setStrCelular2(rs.getString("celular2"));
                        objetoDocente.setStrTitulo1(rs.getString("titulo1_docente"));
                        objetoDocente.setStrTitulo2(rs.getString("titulo2_docente"));
                        objetoDocente.setStrTitulo3(rs.getString("titulo3_docente"));
                        existe = true;
                    }
                } 
            }
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        if (existe){
            result = objetoGson.toJson(objetoDocente);
        }
        return result; 
    }
    
 //MODIFICAR INFORMACIÓN DE UN DOCENTE------------------------------------------
    public String modificarDocente(String json){
        Integer result = -1;
        try{
            usuario objetoUsuario = objetoGson.fromJson(json, usuario.class);
            usuarioAD objetoUsuarioAD = new usuarioAD();
            strSQL = objetoUsuarioAD.getSQLToModificarUsuario(objetoUsuario);
            
            
            
            if(!strSQL.equals("")){
                if(ad.Connectar() == 2){
                    ad.BeginTran();
                    int aux = ad.EjecutarUpdate(strSQL);
                    System.out.println(aux);
                    if (aux != 2){
                        objetoDocente = objetoGson.fromJson(json, docente.class);
                        strSQL = objetoDocenteAD.getSQLToModificarDocente(objetoDocente);
                        result = ad.EjecutarUpdate(strSQL);
                    }
                    ad.CommitTran();
                    ad.Desconectar();
                } 
            }
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        return result.toString();
    }   

//LISTAR LOS PARALELOS DE UN DOCENTE--------------------------------------------    
    public String listarParalelos(String json) {
        String result = "-1";
        Boolean existe = false;
        List<paralelo> lParalelo = new ArrayList<>(); 
        try{
            objetoDocente = objetoGson.fromJson(json, docente.class);
            paraleloAD objetoParaleloAD = new paraleloAD();
            strSQL = objetoParaleloAD.getSQLToListarParalelosDocente(objetoDocente);
            if(!strSQL.equals("")){
                if(ad.Connectar()==2){
                    ad.EjecutarSQL(strSQL);
                    ResultSet rs = ad.getRs();
                    while(rs.next()){
                        paralelo aux = new paralelo();
                        aux.setIntIdParalelo(rs.getInt("id_paralelo"));
                        aux.setIntIdPeriodo(rs.getInt("id_periodo"));
                        aux.setStrParalelo(rs.getString("paralelo"));
                        aux.setStrDescripcion(rs.getString("descripcion_par"));
                        lParalelo.add(aux);
                        existe = true;
                    }
                    ad.CommitTran();
                    ad.Desconectar();
                }
            }
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        if(existe){
            result = objetoGson.toJson(lParalelo);
            System.out.println(result);
        }
        return result;
    }
    
//LÓGICA PARA EL LISTADO DE PARALELOS DE UN DOCENTE DADO LA MATERIA-------------    
    public String listarParalelosMateria(String jsonClase) {
        String result = "-1";
        Boolean existe = false;
        List<paralelo> lParalelo = new ArrayList<>(); 
        try{
            clase objetoclase = objetoGson.fromJson(jsonClase, clase.class);
            strSQL = objetoDocenteAD.getSQLToSelectParalelosForDocenteMateria(objetoclase);
            if(!strSQL.equals("")){
                if(ad.Connectar()==2){
                    ad.EjecutarSQL(strSQL);
                    ResultSet rs = ad.getRs();
                    while(rs.next()){
                        paralelo aux = new paralelo();
                        aux.setIntIdParalelo(rs.getInt("id_paralelo"));
                        aux.setIntIdPeriodo(rs.getInt("id_periodo"));
                        aux.setStrParalelo(rs.getString("paralelo"));
                        aux.setStrDescripcion(rs.getString("descripcion_par"));
                        lParalelo.add(aux);
                        existe = true;
                    }
                }
            }
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        if(existe){
            result = objetoGson.toJson(lParalelo);
            System.out.println(result);
        }
        return result;
    }
    
//LSITAR LAS MATERIAS DE UN DOCENTE---------------------------------------------
    public String listarMateriasDocente(String jsonDocente) {
        String result = "-1";
        Boolean existe = false;
        List<materia> listaMateria = new ArrayList<>();
        materiaAD objetoMateriaAD = new materiaAD();
        try{
            docente objetoDocente = objetoGson.fromJson(jsonDocente, docente.class);
            strSQL = objetoMateriaAD.getSQLToListarMateriasDocente(objetoDocente);
            if(ad.Connectar() == 2){
                ad.BeginTran();
                ad.EjecutarSQL(strSQL);
                ResultSet rs = ad.getRs();
                while(rs.next()){
                    materia aux = new materia();
                    aux.setIntIdMAteria(rs.getInt("id_materia"));
                    aux.setStrNombreMAteria(rs.getString("nombre_mat"));
                    aux.setStrDescripcionMateria(rs.getString("descripcion_mat"));
                    listaMateria.add(aux);
                    existe = true;
                }
                ad.CommitTran();
                ad.Desconectar();
            }
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        if(existe){
            result = objetoGson.toJson(listaMateria);
            System.out.println(result);
        }
        return result;
    }

//MÉTODO PARA LISTAR LAS ACTIVIDADES POR FECHA DE PLAZO DE UN DOCENTE-----------    
    public String listarActividadesFechaU(String json) {
        String result = "-1";
        Boolean existe = false;
        List<actividad> lActividad = new ArrayList<>(); 
        try{
            docente objetoDocente = objetoGson.fromJson(json, docente.class);
            actividadAD objetoActividadAD = new actividadAD();
            strSQL = objetoActividadAD.ListActividadesFechaDocente(objetoDocente);
            if(!strSQL.equals("")){
                if(ad.Connectar()==2){
                    ad.EjecutarSQL(strSQL);
                    ResultSet rs = ad.getRs();
                    while(rs.next()){
                        actividad aux = new actividad();
                        aux.setIntIdActividad(rs.getInt("id_actividad"));
                        aux.setIntIdClase(rs.getInt("idclase"));
                        aux.setStrNombre(rs.getString("nombre_act"));
                        aux.setStrFechaRealizado(rs.getString("f_realizado"));
                        aux.setStrFechaPlazo(rs.getString("f_plazo"));
                        aux.setStrDescripcion(rs.getString("descripcion_act"));
                        aux.setStrTipo(rs.getString("tipo_act"));
                        lActividad.add(aux);
                        existe = true;
                    }
                }
            }
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        if(existe){
            result = objetoGson.toJson(lActividad);
            System.out.println(result);
        }
        return result;
    }
    
/*
 *MÉTODO PARA LISTAR LAS MATERIAS DE UN DOCENTE---------------------------------   
 *  */    
    public String ListarMateriasDocente(String json){
        String result = "-1";
        Boolean existe = false;
        List<materia> materias = new ArrayList<>();
        try{
            objetoDocente = objetoGson.fromJson(json, docente.class);
            materiaAD objetoMateriaAD = new materiaAD();
            strSQL = objetoMateriaAD.getSQLToListarMateriasDocente(objetoDocente);
            if(!strSQL.equals("")){
                if(ad.Connectar()==2){
                    ad.EjecutarSQL(strSQL);
                    ResultSet rs = ad.getRs();
                    System.out.println("Estamos aqui");
                    while(rs.next()){
                        materia aux = new materia();
                        aux.setIntIdMAteria(rs.getInt("id_materia"));
                        aux.setStrNombreMAteria(rs.getString("nombre_mat"));
                        aux.setStrDescripcionMateria(rs.getString("descripcion_mat"));
                        materias.add(aux);
                        existe = true;
                    }
                    ad.Desconectar();
                }
            }
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        if(existe){
            result = objetoGson.toJson(materias);
            System.out.println(result);
        }
        return result;
    }

    public String listarClasesDocente(String jsonDocente) {
        String result = "-1";
        Boolean existe = false;
        JSONArray lista = new JSONArray();
        try{
            objetoDocente = objetoGson.fromJson(jsonDocente, docente.class);
            strSQL = objetoDocenteAD.getSQLToListarClasesDocente(objetoDocente);
            if(ad.Connectar() == 2){
                ad.BeginTran();
                ad.EjecutarSQL(strSQL);
                ResultSet rs = ad.getRs();
                while(rs.next()){
                    JSONObject jsonFila = new JSONObject();
                        clase objetoClase = new clase();
                        objetoClase.setIntIdClase(rs.getInt("idclase"));
                        objetoClase.setIntIdParalelo(rs.getInt("idparaleloclase"));
                        objetoClase.setIntIdMateria(rs.getInt("idmateriaclase"));
                    
                    jsonFila.put("clase", objetoGson.toJson(objetoClase));
                    
                    jsonFila.put("paralelo", "{\"intIdParalelo\":\""+rs.getInt("id_paralelo")+"\",\"strParalelo\":\""+rs.getString("paralelo")+"\"}");
                    
                    jsonFila.put("materia", "{\"intIdMAteria\":\""+rs.getInt("id_materia")+"\",\"strNombreMAteria\":\""+rs.getString("nombre_mat")+"\"}");  
                    
                    lista.put(jsonFila);
                    existe = true;
                }
                
                ad.CommitTran();
                ad.Desconectar();
            }
            
            
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        if(existe){
            result = lista.toString();
        }
        return result;
    }

//LOGÍCA DE NEGOCIO PARA EL LISTADO DE LAS NOTIFICACIONES DE UN DOCENTE---------    
    public String notificacionesCompletasDocente(String jsonDocente) {
        String result = "-1";
        Boolean existe = false;
        JSONArray lista = new JSONArray();
        notificacionAD objetoNotificacionAD = new notificacionAD();
        try{
            docente objetoDocente = objetoGson.fromJson(jsonDocente, docente.class);
            strSQL = objetoNotificacionAD.getSQLToListNotificacionesDocente(objetoDocente);
            if(ad.Connectar() == 2){
                ad.BeginTran();
                ad.EjecutarSQL(strSQL);
                ResultSet rs = ad.getRs();
                while(rs.next()){
                    JSONObject jsonFila = new JSONObject();
                        notificacion objetoNotificacion = new notificacion();
                        objetoNotificacion.setIntIdNotificacion(rs.getInt("id_notificacion"));
                        objetoNotificacion.setIntIdRepresentante(rs.getInt("id"));
                        objetoNotificacion.setIntIdDocente(rs.getInt("tbl_id"));
                        objetoNotificacion.setStrNombreNotificacion(rs.getString("nombre_not"));
                        objetoNotificacion.setStrRespuesta(rs.getString("respuesta"));
                        objetoNotificacion.setStrFechaEnvio(rs.getString("fecha_envio"));
                        objetoNotificacion.setStrFechaRespuesta(rs.getString("fecha_respuesta"));
                        objetoNotificacion.setBooEstadoDocente(true);
                        objetoNotificacion.setBooEstadoRepresentante(false);
                    jsonFila.put("notificacion", objetoGson.toJson(objetoNotificacion));
                    jsonFila.put("representante", "{\"strNombre\":\""+rs.getString("nombrerepresentante")+"\",\"strToken\":\""+rs.getString("tokenrepresentante")+"\"}");
                    jsonFila.put("estudiante", "{\"strNombres\":\""+rs.getString("nombre_est")+"\"}");
                    jsonFila.put("calificacion", "{\"intCalificacion\":\""+rs.getInt("calificacion")+"\"}");
                        actividad objetoActividad = new actividad();
                        objetoActividad.setIntIdActividad(rs.getInt("idactividad")); 
                        objetoActividad.setStrNombre(rs.getString("nombre_act"));
                        objetoActividad.setStrFechaRealizado(rs.getString("f_realizado"));
                        objetoActividad.setStrFechaPlazo(rs.getString("f_plazo"));
                        objetoActividad.setStrDescripcion(rs.getString("descripcion_act"));
                        objetoActividad.setStrTipo(rs.getString("tipo_act"));
                    jsonFila.put("actividad", objetoGson.toJson(objetoActividad));
                    jsonFila.put("materia", "{\"strNombreMAteria\":\""+rs.getString("nombre_mat")+"\"}");
                    jsonFila.put("paralelo", "{\"strParalelo\":\""+rs.getString("paralelo")+"\"}");
                    jsonFila.put("docente", "{\"strNombre\":\""+rs.getString("nombres")+"\", \"strToken\":\""+rs.getString("tokendocente")+"\"}");
                    lista.put(jsonFila);
                    existe = true;
                }
                ad.CommitTran();
                ad.Desconectar();
            }
            
            
            
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        if(existe){
            result = lista.toString();
        }
        return result;
    }

    
    
}

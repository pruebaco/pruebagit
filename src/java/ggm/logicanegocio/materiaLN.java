/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ggm.logicanegocio;

import com.google.gson.Gson;
import ggm.accesodatos.claseAD;
import ggm.accesodatos.cursaAD;
import ggm.accesodatos.estudianteAD;
import ggm.accesodatos.materiaAD;
import ggm.accesodatos.paraleloAD;
import ggm.comun.clase;
import ggm.comun.cursa;
import ggm.comun.docente;
import ggm.comun.materia;
import ggm.comun.paralelo;
import ggm.conexion.AccesoDatos;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Cristhian
 */
public class materiaLN {
    private AccesoDatos ad;
    private Gson objetoGson;
    private String strSQL;
    private materiaAD objetoMateriaAD;
    private materia objetoMateria;
    
    public materiaLN(){
        ad = new AccesoDatos();
        objetoGson = new Gson();
        strSQL = "";
        objetoMateriaAD = new materiaAD();
        objetoMateria = new materia();
    }
    
    
//////////////////
//LÓGICA DE NEGOCIO PARA EL INGRESO DE UNA MATERIA------------------------------
    public String ingresarMateria(String jsonClase, String jsonMateria){
        Integer result = -1;
        Integer auxIdMateria = 0;
        clase objetoClase = objetoGson.fromJson(jsonClase, clase.class);
        claseAD objetoClaseAD = new claseAD();
        
        try{
            objetoMateria = objetoGson.fromJson(jsonMateria, materia.class);
            strSQL = objetoMateriaAD.getSQLToIngresarMateria(objetoMateria);
            
            if(ad.Connectar() == 2){
                ad.BeginTran();
                ad.EjecutarUpdate(strSQL);
                strSQL = objetoMateriaAD.getSQLToBuscarUltimaMateria();
                ad.EjecutarSQL(strSQL);
                ResultSet rs = ad.getRs();
                if(rs.next()){
                    auxIdMateria = rs.getInt("id_materia");
                    objetoClase.setIntIdMateria(auxIdMateria);
                }
                if(auxIdMateria != 0){
                    strSQL = objetoClaseAD.getSQLTOBuscarClaseMateriaNula(objetoClase);
                    ad.EjecutarSQL(strSQL);
                    ResultSet rs2 = ad.getRs();
                    if(rs2.next()){
                        strSQL = objetoClaseAD.getSQLToModificarClase(objetoClase);
                        result = ad.EjecutarUpdate(strSQL);
                    }else{
                        strSQL = objetoClaseAD.getSQLToIngresarClase(objetoClase);
                        result = ad.EjecutarUpdate(strSQL);
                        strSQL = objetoClaseAD.getSQLToBuscarUltimaClase();
                        ad.EjecutarSQL(strSQL);
                        ResultSet rs3 = ad.getRs();
                        Integer auxIdClase = 0;
                        if(rs3.next()){
                            auxIdClase = rs3.getInt("idclase");
                            result = ad.EjecutarUpdate(strSQL);
                        }
                        if(auxIdClase != 0){
                            estudianteAD objetoEstudianteAD = new estudianteAD();
                            paralelo objetoParalelo = new paralelo();
                            objetoParalelo.setIntIdParalelo(objetoClase.getIntIdParalelo());
                            strSQL = objetoEstudianteAD.getSQLToListarEstudiantesDeParalelo(objetoParalelo);
                            ad.EjecutarSQL(strSQL);
                            ResultSet rs4 = ad.getRs();
                            cursaAD objetoCursaAD = new cursaAD();
                            while(rs4.next()){
                                cursa objetoCursa = new cursa();
                                objetoCursa.setIntIdClase(auxIdClase);
                                objetoCursa.setIntIdMatricula(rs4.getInt("idmatricula"));
                                strSQL = objetoCursaAD.getSQLToIngresarCursa(objetoCursa);
                                result = ad.EjecutarUpdate(strSQL);
                            }
                        }
                        
                        
                        //ultima clase ingresada 
                        
                        
                    }
                    
                }
                ad.CommitTran();
                ad.Desconectar();
            }
            
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        
        
        return result.toString();
    }
    
//LÓGICA DE NEGOCIO PARA LISTAR PARALELOS DE UN DOCENTE QUE NO TENGAN MATERIAS
//AL CAMPO IDPERIODO SE LO USA PARA ENVIAR EL CODIGO DE LA CLASE A LA CUAL PERTENECE
    public String listadoParalelosConClaseDeDocente(){
        String result = "-1";
        Boolean existe = false;
        List<paralelo> listaParalelos = new ArrayList<>();
        try{
            
            strSQL = objetoMateriaAD.getSQLToListarParalelosDeDocenteConClase();
            if(ad.Connectar() == 2){
                ad.BeginTran();
                ad.EjecutarSQL(strSQL);
                ResultSet rs = ad.getRs();
                while(rs.next()){
                    paralelo aux = new paralelo();
                    aux.setIntIdParalelo(rs.getInt("id_paralelo"));
                    
                    aux.setIntIdPeriodo(rs.getInt("idclase"));
                    
                    aux.setStrParalelo(rs.getString("paralelo"));
                    aux.setStrDescripcion(rs.getString("descripcion_par"));
                    listaParalelos.add(aux);
                    existe = true;
                }
                ad.CommitTran();
                ad.Desconectar();
            }
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        if(existe){
            result = objetoGson.toJson(listaParalelos);
            System.out.println(result);
        }
        return result;
    }
    
//LISTADO DE MATERIAS CONOCIENDO AL DOCENTE Y EL PARALELO-----------------------
    public String listarMateriasDocenteParalelo(String jsonClase){
        String result = "-1";
        Boolean existe = false;
        JSONArray lista = new JSONArray();
        try{
            clase objetoClase = objetoGson.fromJson(jsonClase, clase.class);
            strSQL = objetoMateriaAD.getSQLToListarMateriasDocenteParalelo(objetoClase);
            if(ad.Connectar() == 2){
                ad.BeginTran();
                ad.EjecutarSQL(strSQL);
                ResultSet rs = ad.getRs();
                while(rs.next()){
                    JSONObject jsonFila = new JSONObject();
                    
                    jsonFila.put("clase", "{\"intIdClase\":\""+rs.getInt("idclase")+"\"}");
                    
                    objetoMateria.setIntIdMAteria(rs.getInt("id_materia"));
                    objetoMateria.setStrNombreMAteria(rs.getString("nombre_mat"));
                    objetoMateria.setStrDescripcionMateria(rs.getString("descripcion_mat"));
                    
                    jsonFila.put("materia", objetoGson.toJson(objetoMateria));
                    lista.put(jsonFila);
                    existe = true;
                }
                
                ad.CommitTran();
                ad.Desconectar();
            }
            
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        if(existe){
            result = lista.toString();
        }
        return result;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ggm.logicanegocio;

import com.google.gson.Gson;
import ggm.accesodatos.notificacionAD;
import ggm.comun.notificacion;
import ggm.conexion.AccesoDatos;
import ggm.accesodatos.notificacionesAD;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;

import java.net.URL;
import java.sql.ResultSet;

import java.util.ArrayList;

import java.util.List;

import java.util.logging.Logger;

import org.json.simple.parser.JSONParser;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


/**
 *
 * @author Cristhian
 */
public class notificacionLN {
    private AccesoDatos ad;
    private notificacionesAD oNotificacionesAD;
    private notificacionAD objetoNotificacionAD;
    private Gson objetoGson;
    private String strSQL;
    
    public notificacionLN(){
        objetoNotificacionAD = new notificacionAD();
        ad = new AccesoDatos();
        oNotificacionesAD = new notificacionesAD();
        objetoGson = new Gson();
        strSQL = "";
    }
    
//MÉTODO PARA ENVIAR UNA NOTIFICACIÓN-------------------------------------------    
    public String enviarNotificacion (String strToken){
        try{
            String respuesta = "";
            
            String clave = "AAAApuEgb48:APA91bFABROD_opPlFslMYjZy1xBMYmaWKMlzMQI1WFr94v6F04NCxL8VPzfL9QsNsBJgzvWwuo5cttPenc_ZCsraC5LEzNbzG0IfACc6ygB2M486u9_Ks0NkwCwIDqkrzyoQz08aS1B";
            URL url = new URL("https://fcm.googleapis.com/fcm/send");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Authorization", "key="+clave);
            //A objeto JSON------------------------------------------------------------
            JSONObject todo = new JSONObject();
            //todo.put("to", "cEhqjAq4iRY:APA91bFnJGwfOVgw6Bu9Hy7KF6tsGn2wUg_-z2BR4HPO-u2-vLE-1TrxvG7pTg8b-aA7M19sI7BxB4uVgZrenFmt_RaBp05QvSN1jTGU7nkUOleygDjcj-qpH4r471VK_ccZOWHh-sgp");
            todo.put("to", strToken);
            JSONObject data = new JSONObject();
                data.put("objeto", "Si exacto esto es una: ");
                data.put("De", "prueba");
            todo.put("data", data);
            //-------------------------------------------------------------------------
            String input = todo.toString();
            OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream());
            wr.write(input);//escribimos
            wr.close();//cerramos la conexion
            BufferedReader rd = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String linea;
            while ((linea = rd.readLine()) != null) {
                respuesta+= linea; //procesamos la salida
            }
            return respuesta;
        }
        catch(IOException e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
            return "Error: "+e;
        } 
    }
    
//MÉTODO PARA LISTAR LAS NOTIFICACIONES DE UN REPRESENTANTE---------------------
    public String listarNotificacionesRepresentante(String json) {
        String result = "-1";
        Boolean existe = true;
        List<notificacion> lNotificacion = new ArrayList<>();
        try{
            strSQL = oNotificacionesAD.getSQLToSelectNotificacionesR(json);
            if(!strSQL.equals("")){
                if(ad.Connectar()==2){
                    ad.EjecutarSQL(strSQL);
                    ResultSet rs = ad.getRs();
                    while(rs.next()){
                        notificacion aux = new notificacion();
                        aux.setIntIdNotificacion(rs.getInt("id_notificacion"));
                        
                        //aux.setStrTipoActividad(rs.getString("tipo_activ"));
                        aux.setStrNombreNotificacion(rs.getString("nombre_not"));
                        aux.setStrRespuesta(rs.getString("respuesta"));
                        aux.setStrFechaEnvio(rs.getString("fecha_envio"));
                        aux.setStrFechaRespuesta(rs.getString("fecha_respuesta"));
                        aux.setIntCodigoActividad(rs.getInt("cod_activ"));
                        lNotificacion.add(aux);
                        existe = true;
                    }
                }
            }
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        if(existe){
            result = objetoGson.toJson(lNotificacion);
            System.out.println(result);
        }
        return result;
    }
    
//MÉTODO PARA LISTAR LAS NOTIFICACIONES DE UN DOCENTE---------------------------
    public String listarNotificacionesDocente(String json) {
//        SimpleDateFormat so = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss"); 
//        so.setTimeZone(TimeZone.getTimeZone("America/Guayaquil"));
//        Date op = new Date();        
//        String ho = so.format(op);
//        System.out.println("La zona horaria es: "+ho);
        
        String result = "-1";
        Boolean existe = false;
        List<notificacion> lNotificacion = new ArrayList<>();
        try{
            strSQL = oNotificacionesAD.getSQLToSelectNotificacionesD(json);
            if(!strSQL.equals("")){
                if(ad.Connectar()==2){
                    ad.EjecutarSQL(strSQL);
                    ResultSet rs = ad.getRs();
                    while(rs.next()){
                        notificacion aux = new notificacion();
                        aux.setIntIdNotificacion(rs.getInt("id_notificacion"));
                       
                        //saux.setStrTipoActividad(rs.getString("tipo_activ"));
                        aux.setStrNombreNotificacion(rs.getString("nombre_not"));
                        aux.setStrRespuesta(rs.getString("respuesta"));
                        aux.setStrFechaEnvio(rs.getString("fecha_envio"));
                        aux.setStrFechaRespuesta(rs.getString("fecha_respuesta"));
                        aux.setIntCodigoActividad(rs.getInt("cod_activ"));
                        lNotificacion.add(aux);
                        existe = true;
                    }
                }
            }
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        if(existe){
            System.out.println(lNotificacion);
            result = objetoGson.toJson(lNotificacion);
            System.out.println(result);
        }
        return result;
    }
    
//MÉTODO PARA INGRESAR UNA NUEVA ACTIVIDAD POR PARTE DEL DOCENTE----------------
    public String ingresarActividadDocente(String json, String lista){
        Integer result = -1;
        
        try{
            strSQL = oNotificacionesAD.getSQLToInsertActividadD(json);
            String SQLaux1 = oNotificacionesAD.getSQLToSelectLastActivity();
            String SQLaux2 = "";
            int SQLaux3 = 0;
            String SQLaux4 = "";
            String strToken = "";
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(lista);
            JSONObject jsonObject = (JSONObject) obj;
            JSONArray msg = (JSONArray) jsonObject.get("id");
            if(!strSQL.equals("") && !SQLaux1.equals("")){
                if(ad.Connectar() == 2){
                    ad.BeginTran();
                   
                    result = ad.EjecutarUpdate(strSQL);
                    if(result != -1){
                        ad.EjecutarSQL(SQLaux1);
                        ResultSet rs = ad.getRs();
                        if(rs.next()){
                            SQLaux3 = rs.getInt("id_actividad");
                        }
                    }
                    System.out.println("id Actividad: "+SQLaux3);
                    for (Object object : msg) { //Recuperamos la consulta por cada estudiante que ha sido enviada la tarea 
                        SQLaux2 = oNotificacionesAD.getSQLToAsignarActividadAE(SQLaux3, Integer.parseInt(object.toString()));
                        result = ad.EjecutarUpdate(SQLaux2);
                        System.out.println("Query: "+SQLaux2);
                        if(result != -1){
                            SQLaux4 = oNotificacionesAD.getSQLToSelectTokenRepresentantes(Integer.parseInt(object.toString()));
                            
                            ad.EjecutarSQL(SQLaux4);
                            ResultSet rs2 = ad.getRs();
                            if(rs2.next()){
                                strToken = rs2.getString("tokenmovil");
                                System.out.println("Consulta token: "+strToken);
                                String no = enviarNotificacion(strToken);
                                System.out.println(no);
                            }
                        }   
                    }
                    ad.CommitTran();
                    ad.Desconectar();
                }
            }
            
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        return "";
    }

//LÓGICA DE NEGOCIO PARA LISTAR TODAS LAS NOTIFICACIONES DE UN REPRESENTANTE----
//    public String notificacionesCompletasRepresentante(String jsonRepresentante) {
//        String result = "-1";
//        Boolean existe = false;
//        JSONArray lista = new JSONArray();
//        try{
//            representante objetorepresentante = objetoGson.fromJson(jsonRepresentante, representante.class);
//            strSQL = objetoNotificacionAD.getSQLToListNotificacionesRepresentantes(objetorepresentante);
//            if(ad.Connectar() == 2){
//                ad.BeginTran();
//                ad.EjecutarSQL(strSQL);
//                ResultSet rs = ad.getRs();
//                
//                while(rs.next()){
//                    JSONObject jsonFila = new JSONObject();
//                        notificacion objetoNotificacion = new notificacion();
//                        objetoNotificacion.setIntIdNotificacion(rs.getInt("id_notificacion"));
//                        objetoNotificacion.setIntIdRepresentante(rs.getInt("id"));
//                        objetoNotificacion.setIntIdDocente(rs.getInt("tbl_id"));
//                        objetoNotificacion.setStrNombreNotificacion(rs.getString("nombre_not"));
//                        objetoNotificacion.setStrRespuesta(rs.getString("respuesta"));
//                        objetoNotificacion.setStrFechaEnvio(rs.getString("fecha_envio"));
//                        objetoNotificacion.setStrFechaRespuesta(rs.getString("fecha_respuesta"));
//                        objetoNotificacion.setBooEstadoDocente(false);
//                        objetoNotificacion.setBooEstadoRepresentante(true);
//                    jsonFila.put("notificacion", objetoGson.toJson(objetoNotificacion));
//                        
//                    jsonFila.put("representante", "{\"strNombre\":\""+rs.getString("nombrerepresentante")+"\"}");
//                    
//                    jsonFila.put("estudiante", "{\"strNombres\":\""+rs.getString("nombre_est")+"\"}");
//                    
//                    jsonFila.put("calificacion", "{\"intCalificacion\":\""+rs.getInt("calificacion")+"\"}");
//                    
//                        actividad objetoActividad = new actividad();
//                        objetoActividad.setStrNombre(rs.getString("nombre_act"));
//                        objetoActividad.setStrFechaRealizado(rs.getString("f_realizado"));
//                        objetoActividad.setStrFechaPlazo(rs.getString("f_plazo"));
//                        objetoActividad.setStrDescripcion(rs.getString("descripcion_act"));
//                        objetoActividad.setStrTipo(rs.getString("tipo_act"));
//                    jsonFila.put("actividad", objetoGson.toJson(objetoActividad));
//                    
//                    jsonFila.put("materia", "{\"strNombreMAteria\":\""+rs.getString("nombre_mat")+"\"}");
//                    
//                    jsonFila.put("paralelo", "{\"strParalelo\":\""+rs.getString("paralelo")+"\"}");
//                    
//                    jsonFila.put("docente", "{\"strNombre\":\""+rs.getString("nombres")+"\"}");
//                    
//                    lista.put(jsonFila);
//                    
//                    existe = true;
//                }
//                
//                ad.CommitTran();
//                ad.Desconectar();
//            }
//            
//        }catch(Exception e){
//            Logger log = Logger.getLogger(this.getClass().getName());
//            log.severe(e.getMessage());
//            ad.RollbackTran();
//        }
//        if(existe){
//            result = lista.toString();
//        }
//        return result;
//    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ggm.logicanegocio;

import ggm.accesodatos.evaluacionAD;
import com.google.gson.Gson;
import ggm.accesodatos.cursaAD;
import ggm.accesodatos.estudianteAD;
import ggm.comun.clase;
import ggm.comun.cursa;
import ggm.comun.estudiante;
import ggm.comun.evaluacion;
import ggm.comun.matricula;
import ggm.comun.parcial;
import ggm.conexion.AccesoDatos;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Cristhian
 */
public class evaluacionLN {
    private AccesoDatos ad;
    private Gson objetoGson;
    private String strSQL;
    private evaluacionAD objetoEvaluacionAD;
    private evaluacion objetoEvaluacion;
    
    
    public evaluacionLN(){
        ad = new AccesoDatos();
        objetoGson = new Gson();
        strSQL = "";
        objetoEvaluacionAD = new evaluacionAD();
        objetoEvaluacion = new evaluacion();
    }

//LÓGICA DE NEGOCIO PARA INGRESAR UNA EVALUACIÓN DE UN PARCIAL------------------    
    public String ingresoEvaluacion(String jsonClase, String jsonMatricula, String jsonParcial, String jsonEvaluacion) {
        Integer result = -1;
        try{
            cursa objetoCursa = new cursa();
            clase objetoClase = objetoGson.fromJson(jsonClase, clase.class);
            matricula objetoMatricula = objetoGson.fromJson(jsonMatricula, matricula.class);
            
            objetoCursa.setIntIdMatricula(objetoMatricula.getIntIdMatricula());
            objetoCursa.setIntIdClase(objetoClase.getIntIdClase());
            
            cursaAD objetoCursaAD = new cursaAD();
            System.out.println("DATOS DE CURSA "+objetoCursa.getIntIdMatricula()+"  "+objetoCursa.getIntIdClase());
            
            System.out.println("INF-10");
            parcial objetoParcial = objetoGson.fromJson(jsonParcial, parcial.class);
            evaluacion objetoEvaluacion = objetoGson.fromJson(jsonEvaluacion, evaluacion.class);
            strSQL = objetoCursaAD.getSQLToBuscarIdCursa(objetoCursa);
            
            System.out.println("INF0");
            if(!strSQL.equals("")){
                if(ad.Connectar() == 2){
                    ad.BeginTran();
                    
                    
                    ad.EjecutarSQL(strSQL);
                    ResultSet rs = ad.getRs();
                    if(rs.next()){
                        objetoCursa.setIntIdCursa(rs.getInt("idcursa"));
                        System.out.println("INF01");
                    }
                    if(objetoCursa.getIntIdCursa() != 0){
                        if(objetoEvaluacion.getIntIdEvaluacion() != 0 ){
                            strSQL = objetoEvaluacionAD.getSQLToModificarEvaluacion(objetoEvaluacion);
                            System.out.println("INF1");
                        }else{
                            strSQL = objetoEvaluacionAD.getSQLToIngresarEvaluacion(objetoCursa, objetoParcial, objetoEvaluacion);
                        }
                        result = ad.EjecutarUpdate(strSQL);
                    }
                    ad.CommitTran();
                    ad.Desconectar();
                }
            }
        }
        catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        return result.toString();
    }

//LÓGICA DE NEGOCIO PARA LISTAR LOS ESTUDIANTES DE UNA CLASE CON SUS NOTAS DE UN
//DETERMINADO PARCIAL-----------------------------------------------------------
//A CAMBIO DE LA EDAD VA EL ID DE LA MATRICULA QUE TIENE EL ESTUDIANTE----------
    public String listarEstudiantesParalelo(String jsonClase, String jsonParcial) {
        String result = "-1";
        Boolean existe = false;
        estudianteAD objetoEstudianteAD = new estudianteAD();
        JSONArray lista = new JSONArray();
        try{
            System.out.println("LLegamos al Try: ");
            clase objetoClase = objetoGson.fromJson(jsonClase, clase.class);
            parcial objetoParcial = objetoGson.fromJson(jsonParcial, parcial.class);
            
            strSQL = objetoEstudianteAD.getSQLToListarEstudiantesDeClase(objetoClase, objetoParcial);
            if(!strSQL.equals("")){
                if(ad.Connectar()==2){
                    ad.BeginTran();
                    ad.EjecutarSQL(strSQL);
                    ResultSet rs = ad.getRs();
                    while(rs.next()){
                        JSONObject jsonFila = new JSONObject();
                        
                        estudiante aux = new estudiante();
                        aux.setIntIdEstudiante(rs.getInt("id_estudiante"));
                        aux.setIntIdRepresentante(rs.getInt("id"));
                        aux.setStrApellidos(rs.getString("apellido_est"));
                        aux.setStrNombres(rs.getString("nombre_est"));
                        aux.setStrCedula(rs.getString("cedula_est"));
                        aux.setIntEdad(rs.getInt("idmatricula"));
                        
                        jsonFila.put("estudiante", objetoGson.toJson(aux));
                        
                        evaluacion objetoEvaluacion = new evaluacion();
                        objetoEvaluacion.setIntIdEvaluacion(rs.getInt("idnota"));
                        objetoEvaluacion.setIntIdParcial(rs.getInt("idparcial"));
                        objetoEvaluacion.setIntIdCursa(rs.getInt("idcursa"));
                        objetoEvaluacion.setIntNota1(rs.getInt("nota1"));
                        objetoEvaluacion.setIntNota2(rs.getInt("nota2"));
                        objetoEvaluacion.setIntNota3(rs.getInt("nota3"));
                        objetoEvaluacion.setIntNota4(rs.getInt("nota4"));
                        
                        jsonFila.put("evaluacion", objetoGson.toJson(objetoEvaluacion));
                        
                        lista.put(jsonFila);
                        existe = true;
                    }
                    ad.CommitTran();
                    ad.Desconectar();
                }
            }        
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        if(existe){
            result = lista.toString();
            System.out.println("Lista respuesta: "+result);
        }
        return result;
    }

    
    
    




}

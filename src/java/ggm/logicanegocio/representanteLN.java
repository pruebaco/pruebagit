/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ggm.logicanegocio;

import ggm.accesodatos.representanteAD;
import com.google.gson.Gson;
import ggm.accesodatos.notificacionAD;
import ggm.accesodatos.usuarioAD;
import ggm.comun.actividad;
import ggm.comun.notificacion;
import ggm.comun.representante;
import ggm.comun.usuario;
import ggm.conexion.AccesoDatos;
import java.sql.ResultSet;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Cristhian
 */
public class representanteLN {
    private AccesoDatos ad;
    private Gson objetoGson;
    private String strSQL;
    private representanteAD RepresentanteAD;
    private representante Representante;
    
    public representanteLN(){
        ad = new AccesoDatos();
        objetoGson = new Gson();
        strSQL = "";
        RepresentanteAD = new representanteAD();
        Representante = new representante();
    }

//MOSTRAR INFORMACION DE UN REPRESENTANTE--------------------------------------
    public String perfilRepresentante(String json){
        String result = "-1";
        Boolean existe = false;
        try{
            representante objetoRepresentante = objetoGson.fromJson(json, representante.class);
            strSQL = RepresentanteAD.getSQLToBuscarRepresentante(objetoRepresentante);
            if(!strSQL.equals("")){
                if(ad.Connectar() == 2){
                    
                    ad.EjecutarSQL(strSQL);
                    ResultSet rs = ad.getRs();
                    if(rs.next()){
                        Representante.setIntIdUsuario(rs.getInt("id"));
                        Representante.setStrNombre(rs.getString("nombres"));
                        Representante.setStrApellido(rs.getString("apellidos"));
                        Representante.setStrCedula(rs.getString("cedula"));
                        Representante.setIntEdad(rs.getInt("edad"));
                        Representante.setStrCorreo(rs.getString("correo"));
                        Representante.setStrTelefono(rs.getString("telefono"));
                        Representante.setStrCelular1(rs.getString("celular1"));
                        Representante.setStrCelular2(rs.getString("celular2"));
                        Representante.setStrNivelAca(rs.getString("nivel_aca"));
                        Representante.setStrDireccion(rs.getString("direccion"));
                        
                        existe = true;
                    }
                    ad.Desconectar();
                } 
            }
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        if (existe){
            result = objetoGson.toJson(Representante);
        }
        return result;
    }
    
//MODIFICAR INFORMACIÓN DE UN REPRESENTANTE-------------------------------------
    public String modificarRepresentante(String json){
        Integer result = -1;
        try{
            usuario objetoUsuario = objetoGson.fromJson(json, usuario.class);
            usuarioAD objetoUsuarioAD = new usuarioAD();
            strSQL = objetoUsuarioAD.getSQLToModificarUsuario(objetoUsuario);
            representante objetoRepresentante = objetoGson.fromJson(json, representante.class);
            if(!strSQL.equals("")){
                if(ad.Connectar() == 2){
                    ad.BeginTran();
                    result = ad.EjecutarUpdate(strSQL);
                    if(result == 1){
                        strSQL = RepresentanteAD.getSQLToModificarRepresentante(objetoRepresentante);
                        result = ad.EjecutarUpdate(strSQL);
                    }
                    ad.CommitTran();
                    ad.Desconectar();
                } 
            }
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        return result.toString();
    }
    
    
//LÓGICA DE NEGOCIO PARA LISTAR TODAS LAS NOTIFICACIONES DE UN REPRESENTANTE----
    public String notificacionesCompletasRepresentante(String jsonRepresentante) {
        System.out.println("LLEGO A LA FUNCION");
        String result = "-1";
        Boolean existe = false;
        JSONArray lista = new JSONArray();
        notificacionAD objetoNotificacionAD = new notificacionAD();
        try{
            representante objetorepresentante = objetoGson.fromJson(jsonRepresentante, representante.class);
            strSQL = objetoNotificacionAD.getSQLToListNotificacionesRepresentantes(objetorepresentante);
            if(ad.Connectar() == 2){
                ad.BeginTran();
                ad.EjecutarSQL(strSQL);
                ResultSet rs = ad.getRs();
                while(rs.next()){
                    JSONObject jsonFila = new JSONObject();
                        notificacion objetoNotificacion = new notificacion();
                        objetoNotificacion.setIntIdNotificacion(rs.getInt("id_notificacion"));
                        objetoNotificacion.setIntIdRepresentante(rs.getInt("id"));
                        objetoNotificacion.setIntIdDocente(rs.getInt("tbl_id"));
                        objetoNotificacion.setStrNombreNotificacion(rs.getString("nombre_not"));
                        objetoNotificacion.setStrRespuesta(rs.getString("respuesta"));
                        objetoNotificacion.setStrFechaEnvio(rs.getString("fecha_envio"));
                        objetoNotificacion.setStrFechaRespuesta(rs.getString("fecha_respuesta"));
                        objetoNotificacion.setBooEstadoDocente(false);
                        objetoNotificacion.setBooEstadoRepresentante(true);
                    jsonFila.put("notificacion", objetoGson.toJson(objetoNotificacion));
                    jsonFila.put("representante", "{\"strNombre\":\""+rs.getString("nombrerepresentante")+"\",\"strToken\":\""+rs.getString("tokenrepresentante")+"\"}");
                    jsonFila.put("estudiante", "{\"strNombres\":\""+rs.getString("nombre_est")+"\"}");
                    jsonFila.put("calificacion", "{\"intCalificacion\":\""+rs.getInt("calificacion")+"\"}");
                        actividad objetoActividad = new actividad();
                        objetoActividad.setIntIdActividad(rs.getInt("idactividad")); 
                        objetoActividad.setStrNombre(rs.getString("nombre_act"));
                        objetoActividad.setStrFechaRealizado(rs.getString("f_realizado"));
                        objetoActividad.setStrFechaPlazo(rs.getString("f_plazo"));
                        objetoActividad.setStrDescripcion(rs.getString("descripcion_act"));
                        objetoActividad.setStrTipo(rs.getString("tipo_act"));
                    jsonFila.put("actividad", objetoGson.toJson(objetoActividad));
                    jsonFila.put("materia", "{\"strNombreMAteria\":\""+rs.getString("nombre_mat")+"\"}");
                    jsonFila.put("paralelo", "{\"strParalelo\":\""+rs.getString("paralelo")+"\"}");
                    jsonFila.put("docente", "{\"strNombre\":\""+rs.getString("nombres")+"\", \"strToken\":\""+rs.getString("tokendocente")+"\"}");
                    lista.put(jsonFila);
                    existe = true;
                }
                ad.CommitTran();
                ad.Desconectar();
            }
            
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        if(existe){
            result = lista.toString();
        }
        return result;
    }
    
    
}

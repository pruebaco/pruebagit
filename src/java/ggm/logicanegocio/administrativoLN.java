/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ggm.logicanegocio;

import com.google.gson.Gson;
import ggm.accesodatos.usuarioAD;
import ggm.accesodatos.administrativoAD;
import ggm.comun.administrativo;
import ggm.comun.usuario;
import ggm.conexion.AccesoDatos;
import java.sql.ResultSet;
import java.util.logging.Logger;


/**
 *
 * @author Cristhian
 */
public class administrativoLN {
    private AccesoDatos ad;
    private Gson objetoGson;
    private String strSQL;
    private administrativoAD objetoAdministrativoAD;
    private administrativo objetoAdministrativo;
    
    public administrativoLN(){
        ad = new AccesoDatos();
        objetoGson = new Gson();
        strSQL = "";
        objetoAdministrativoAD = new administrativoAD();
        objetoAdministrativo = new administrativo();
    }
    
//MOSTRAR INFORMACION DE UN ADMINSITRATIVO--------------------------------------    
    public String perfilAdminsitrativo(String json){
        String result = "-1";
        Boolean existe = false;
        try{
            usuario objetoUsuario = new usuario();
            objetoUsuario = objetoGson.fromJson(json, usuario.class);
            strSQL = objetoAdministrativoAD.getSQLToBuscarAdministrativo(objetoUsuario);
            if(!strSQL.equals("")){
                if(ad.Connectar() == 2){
                    ad.EjecutarSQL(strSQL);
                    ResultSet rs = ad.getRs();
                    if(rs.next()){
                        objetoAdministrativo.setStrNombre(rs.getString("nombres"));
                        objetoAdministrativo.setStrApellido(rs.getString("apellidos"));
                        objetoAdministrativo.setStrCedula(rs.getString("cedula"));
                        objetoAdministrativo.setIntEdad(rs.getInt("edad"));
                        objetoAdministrativo.setStrCorreo(rs.getString("correo"));
                        objetoAdministrativo.setStrTelefono(rs.getString("telefono"));
                        objetoAdministrativo.setStrCelular1(rs.getString("celular1"));
                        objetoAdministrativo.setStrCelular2(rs.getString("celular2"));
                        objetoAdministrativo.setStrCargo(rs.getString("cargo"));
                        objetoAdministrativo.setStrTitulo1(rs.getString("titulo1_adm"));
                        objetoAdministrativo.setStrTitulo2(rs.getString("titulo2_adm"));
                        existe = true;
                    }
                } 
            }
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        if (existe){
            result = objetoGson.toJson(objetoAdministrativo);
            //System.out.println(result);
        }
        return result;
        
    }
    
//MODIFICAR INFORMACIÓN DE UN ADMINISTRATIVO-------------------------------------
    public String modificarAdministrativo(String json){
        Integer result = -1;
        try{
            usuarioAD objetoUsuarioAD = new usuarioAD();
            usuario objetoUsuario = new usuario();
            objetoUsuario = objetoGson.fromJson(json, usuario.class);
            strSQL = objetoUsuarioAD.getSQLToModificarUsuario(objetoUsuario);
            if(!strSQL.equals("")){
                if(ad.Connectar() == 2){
                    ad.BeginTran();
                    if(ad.EjecutarUpdate(strSQL) != 2){
                        objetoAdministrativo = objetoGson.fromJson(json, administrativo.class);
                        strSQL = objetoAdministrativoAD.getSQLToModificarAdministrativo(objetoAdministrativo);
                        result = ad.EjecutarUpdate(strSQL); 
                    }
                    ad.CommitTran();
                    ad.Desconectar();
                } 
            }
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        return result.toString();
    }
    
}

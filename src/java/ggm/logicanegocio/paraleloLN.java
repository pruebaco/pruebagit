/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ggm.logicanegocio;

import ggm.accesodatos.paraleloAD;
import com.google.gson.Gson;
import ggm.accesodatos.claseAD;
import ggm.comun.clase;
import ggm.comun.docente;
import ggm.comun.paralelo;
import ggm.conexion.AccesoDatos;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author Cristhian
 */
public class paraleloLN {
    private String strSQL;
    private paraleloAD objetoParaleloAD;
    private AccesoDatos ad;
    private Gson objetoGson;
    private paralelo objetoParalelo;
    
    public paraleloLN(){
        strSQL = "";
        objetoParaleloAD = new paraleloAD();
        ad = new AccesoDatos();
        objetoGson = new Gson();
        objetoParalelo = new paralelo();
    }
    
    //MÉTODO PARA EL LISTADO DE LOS PARALELOS DE LA INSTITUCION--------------------
    public String listarParalelosGeneral() {
        String result = "-1";
        Boolean existe = false;
        List<paralelo> listaParalelos = new ArrayList<>();
        try{
            strSQL = objetoParaleloAD.getSQLToListarTodosParalelos();
            if(!strSQL.equals("")){
                if(ad.Connectar() == 2){
                    ad.EjecutarSQL(strSQL);
                    ResultSet rs = ad.getRs();
                    while(rs.next()){
                        paralelo aux = new paralelo();
                        aux.setIntIdParalelo(rs.getInt("id_paralelo"));
                        aux.setIntIdPeriodo(rs.getInt("id_periodo"));
                        aux.setStrParalelo(rs.getString("paralelo"));
                        aux.setStrDescripcion(rs.getString("descripcion_par"));
                        listaParalelos.add(aux);
                        existe = true;
                    }
                }
            }
            
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        if(existe){
            result = objetoGson.toJson(listaParalelos);
        }
        return result;
    }
    
//LOGICA PARA EL INGRESO DE UN NUEVO PARALELO-----------------------------------
    public String ingresarParaleloDocente(String jsonDocente, String jsonParalelo) {
        Integer result = -1;
        Integer idParaleloAux = 0;
        try{
            objetoParalelo = objetoGson.fromJson(jsonParalelo, paralelo.class);
            docente objetoDocente = objetoGson.fromJson(jsonDocente, docente.class);
            objetoParalelo.setIntIdPeriodo(objetoDocente.getIntPeriodo());
            strSQL = objetoParaleloAD.getSQLToRegistrar(objetoParalelo);
            if(ad.Connectar() == 2){
                ad.BeginTran();
                if(!strSQL.equals("")){
                    result = ad.EjecutarUpdate(strSQL);
                    strSQL = objetoParaleloAD.getSQLToLastParalelo();
                    ad.EjecutarSQL(strSQL);
                    ResultSet rs = ad.getRs();
                    if(rs.next()){
                        idParaleloAux = rs.getInt("id_paralelo");
                    }
                    if(idParaleloAux != 0){
                        clase objetoClase = new clase();
                        objetoClase.setIntIdDocente(objetoDocente.getIntIdUsuario());
                        objetoClase.setIntIdParalelo(idParaleloAux);
                        claseAD objetoClaseAD = new claseAD();
                        strSQL = objetoClaseAD.getSQLToIngresarClase(objetoClase);
                        result = ad.EjecutarUpdate(strSQL);
                        result = idParaleloAux;
                    }else result = -1;
                    ad.CommitTran();
                    ad.Desconectar();   
                }
            }
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            result = -1;
            ad.RollbackTran();
        }
        return result.toString();
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ggm.logicanegocio;

import com.google.gson.Gson;
import ggm.accesodatos.periodoacademicoAD;
import ggm.comun.paralelo;
import ggm.comun.periodo;
import ggm.conexion.AccesoDatos;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 *
 * @author Cristhian
 */
public class periodoacademicoLN {
    private AccesoDatos ad;
    private periodoacademicoAD objetoPeriodoAD;
    private Gson objetoGson;
    private String strSQL;
    private String resultado;
    
    public periodoacademicoLN(){
        ad = new AccesoDatos();
        objetoPeriodoAD = new periodoacademicoAD();
        objetoGson = new Gson();
        strSQL = "";
        resultado = "0";
    }
    
    public String ingresarPeriodoAcademico(String json) {
        resultado = "-1";
        try{
            periodo objetoPeriodo = new periodo();
            objetoPeriodo = objetoGson.fromJson(json, periodo.class);
            strSQL = objetoPeriodoAD.getSQLToIngresarPeriodo(objetoPeriodo);
            if(ad.Connectar() == 2){
                ad.BeginTran();
                if(!strSQL.equals("")){
                    resultado = ad.EjecutarUpdate(strSQL).toString();
                    System.out.println("Usuario respuesta: "+resultado);
                }
                ad.CommitTran();
                ad.Desconectar();
            }
        }
        catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        return resultado.toString();
    }

    public String listarPeriodosAcademicos() {
        Boolean existe = false;
        List<periodo> listaPeriodos = new ArrayList<>();
        try{
            strSQL = objetoPeriodoAD.getSQLToListarPeriodos();
            if(!strSQL.equals("")){
                if(ad.Connectar() == 2){
                    ad.EjecutarSQL(strSQL);
                    ResultSet rs = ad.getRs();
                    while(rs.next()){
                        periodo aux = new periodo();
                        aux.setIntIdPeriodo(rs.getInt("id_periodo"));
                        aux.setStrPeriodoAcademico(rs.getString("periodo_aca"));
                        listaPeriodos.add(aux);
                        existe = true;
                    }
                }
            }
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ad.RollbackTran();
        }
        if(existe){
            resultado = objetoGson.toJson(listaPeriodos);
        }
        return resultado;
    }
    
}

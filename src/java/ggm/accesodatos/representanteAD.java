/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ggm.accesodatos;

import com.google.gson.Gson;
import ggm.comun.estudiante;
import ggm.comun.representante;
import ggm.comun.usuario;
import ggm.conexion.AccesoDatos;
import java.util.Base64;
import java.util.logging.Logger;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author Cristhian
 */
public class representanteAD {
    //private representante Representante;
    private usuario Usuario;
    private representante oRepresentante;
    private Gson gson;
    private AccesoDatos ac;
    private String resultado;
    
    public representanteAD(){
        //Representante = new representante();
        Usuario = new usuario();
        oRepresentante = new representante();
        gson = new Gson();
        ac = new AccesoDatos();
        resultado = "";
    }
  
//MOSTRAR LA INFORMACION DE UN REPRESENTANTE----------------------------------
    public String getSQLToBuscarRepresentante(representante objeto){
        if(objeto != null){
            resultado = "select *\n"
                    + "from tblrepresentante\n"
                    + "where cedula = '"+ objeto.getStrCedula()+"'\n";
        }
        System.out.println(resultado);
        return resultado;
    }
    
//MODIFICAR LA INFORMACION DE UN REPRESENTANTE----------------------------------    
    public String getSQLToModificarRepresentante(representante objeto){
        if(objeto != null){
            resultado = "UPDATE tblrepresentante SET nombres='"+objeto.getStrNombre()+"', apellidos= '"+objeto.getStrApellido()+"', edad="
             +objeto.getIntEdad()+", correo='"+objeto.getStrCorreo()+"', telefono='"           
             +objeto.getStrTelefono()+"', celular1='"+objeto.getStrCelular1()+"', nivel_aca='"+objeto.getStrNivelAca()+"', direccion='"
             +objeto.getStrDireccion()+"' WHERE (cedula = '"+objeto.getStrCedula()+"');";
        }
        System.out.println(resultado);
        return resultado;
    }
    
//Modifica la información de la tabla usuario-----------------------------------
    public Integer getSQLToUpdate2(String json) {
        try{
        oRepresentante = gson.fromJson(json, representante.class);
//        byte [] bytFoto = Base64.getDecoder().decode(oRepresentante.getStrFoto());
        Integer aux=0;
        String sql = "UPDATE tblusuario SET nombres='"+oRepresentante.getStrNombre()+"', apellidos= '"+oRepresentante.getStrApellido()+"', edad="
                +oRepresentante.getIntEdad()+", correo='"+oRepresentante.getStrCorreo()+"', telefono='"+oRepresentante.getStrTelefono()+"', celular1='"           
                +oRepresentante.getStrCelular1()+"' WHERE (cedula = '"+oRepresentante.getStrCedula()+"');";
        System.out.println(sql);
        if(ac.Connectar()==2){
             ac.BeginTran();
            aux = ac.EjecutarUpdate(sql);
            System.out.println("Si");
            ac.CommitTran();
            ac.Desconectar();
            return aux;    
        }else {return aux;}
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ac.RollbackTran();
            return (2);
            }
        }

//ACCESO A DATOS PARA REGISTRAR A UN REPRESENTANTE------------------------------
    public String getSQLToIngresarRepresentante(representante objeto){ 
        if(objeto != null){
            resultado = "INSERT INTO tblrepresentante(\n"
                        + "id, nombres, apellidos, cedula, edad, correo, telefono, celular1, contrasena, id_periodo, tipo, nivel_aca)\n"
                        + "VALUES ("+objeto.getIntIdUsuario()+", '"+objeto.getStrNombre()+"', '"+objeto.getStrApellido()+"', '"+objeto.getStrCedula()+"',"
                        +" "+objeto.getIntEdad()+", '"+objeto.getStrCorreo()+"', '"+objeto.getStrTelefono()+"',"
                        +"'"+objeto.getStrCelular1()+"', '"+objeto.getStrContrasena()+"', "+objeto.getIntPeriodo()+", '"+objeto.getStrTipo()+"', '"+objeto.getStrNivelAca()+"');";
        }  
        return resultado;
    }
    
//ACCESO A DATOS PARA ASIGNAR EL TOKEN A UN REPRESENTATNE-----------------------
    public String getSQLToAsignarTokenRepresentante(representante objeto){
        if(objeto != null){
            resultado = "UPDATE tblrepresentante SET tokenmovil='"+objeto.getStrToken()+"' WHERE (cedula ='"+objeto.getStrCedula()+"')";
            System.out.println(resultado);
        }
        return resultado;
    }
    
    //MÉTODO PARA RECUPERAR EL TOKEN DEL REPRESENTANTE DE UN ESTUDIANTE-------------    
    public String getSQLToBuscarRepresentanteDeEstudiante(estudiante objeto) {
        if(objeto != null){
            resultado = "select * from tblestudiante as te inner join tblrepresentante as tr on te.id = tr.id where te.id_estudiante = "+objeto.getIntIdEstudiante()+"";
            System.out.println(resultado);
        }
        return resultado;
    }
    
}


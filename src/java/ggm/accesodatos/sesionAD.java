/*
 * SESIONAD.JAVA To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ggm.accesodatos;

import com.google.gson.Gson;
import ggm.comun.login;
import ggm.comun.periodo;
import ggm.comun.usuario;
import ggm.conexion.AccesoDatos;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Cristhian
 */
public class sesionAD {
   private login Login;
   private Gson oGson;
   private usuario oUsuario;
   private periodo oPeriodo;
   private AccesoDatos ac;
   private String resultado = ""; 
   
   public sesionAD(){
       Login = new login();
       oGson = new Gson();
       oUsuario = new usuario();
       oPeriodo = new periodo();
       ac = new AccesoDatos();
   }
   
//MÉTODO PARA EL LOGEO DE LOS USUARIOS------------------------------------------
   public String getSQLToLogin(String json){
       String result = "";
       if(!json.equals("")){
           Gson gson = new Gson();
           Login = gson.fromJson(json, login.class);
           result = "SELECT *\n"
                    + "FROM tblUsuario\n"
                    + "WHERE cedula = '" + Login.getStrCedula() + "'\n" 
                    + "AND contrasena = '"+Login.getStrContrasena()+"';";
       }
       System.out.println(result);
       return result;
   }
   
//MÉTODO PARA EL REGISTRO DE LOS USUARIOS--------------------------------------- 
//   public String getSQLToRegistrar(String json, Integer periodo){
//       String result = "";
//       if(!json.equals("")){
//           oUsuario = oGson.fromJson(json, usuario.class);
//           result = "INSERT INTO tblusuario(\n"
//                    + "nombres, apellidos, cedula, edad, correo, telefono, celular1, contrasena, id_periodo, tipo)\n"
//                    + "VALUES ('"+oUsuario.getStrNombre()+"', '"+oUsuario.getStrApellido()+"', '"+oUsuario.getStrCedula()+"',"
//                    +" "+oUsuario.getIntEdad()+", '"+oUsuario.getStrCorreo()+"', '"+oUsuario.getStrTelefono()+"',"
//                    +"'"+oUsuario.getStrCelular1()+"', '"+oUsuario.getStrContrasena()+"', "+periodo+", '"+oUsuario.getStrTipo()+"');";
//       }
//       System.out.println(result);
//       return result;
//   }

//MÉTODO PARA EL REGISTRO SEGUN EL TIPO DE USUARIO------------------------------
//   public String SQLToRegistrarTipo(String json, Integer periodo, AccesoDatos ad){
//       Integer result = -1;
//       String sql = "";
//       String aux = "";
//       try{
//           oUsuario = oGson.fromJson(json, usuario.class);
//           sql = "select id from tblusuario where cedula ='"+oUsuario.getStrCedula()+"'";
//                ad.EjecutarSQL(sql);
//                ResultSet rs = ad.getRs();
//                if(rs.next()){
//                    aux = rs.getString("id");
//                }
//                System.out.println(aux);
//               switch(oUsuario.getStrTipo()){
//               case "Representante":
//                   sql = "INSERT INTO tblrepresentante(\n"
//                    + "id, nombres, apellidos, cedula, edad, correo, telefono, celular1, contrasena, id_periodo, tipo, nivel_aca)\n"
//                    + "VALUES ('"+aux+"', '"+oUsuario.getStrNombre()+"', '"+oUsuario.getStrApellido()+"', '"+oUsuario.getStrCedula()+"',"
//                    +" "+oUsuario.getIntEdad()+", '"+oUsuario.getStrCorreo()+"', '"+oUsuario.getStrTelefono()+"',"
//                    +"'"+oUsuario.getStrCelular1()+"', '"+oUsuario.getStrContrasena()+"', "+periodo+", '"+oUsuario.getStrTipo()+"', ' ');";
//                   ad.EjecutarUpdate(sql);
//                   System.out.println(1);
//                   break;
//               case "Docente":
//                   sql = "INSERT INTO tbldocente(\n"
//                    + "id, nombres, apellidos, cedula, edad, correo, telefono, celular1, contrasena, id_periodo, tipo, titulo1_docente)\n"
//                    + "VALUES ('"+aux+"', '"+oUsuario.getStrNombre()+"', '"+oUsuario.getStrApellido()+"', '"+oUsuario.getStrCedula()+"',"
//                    +" "+oUsuario.getIntEdad()+", '"+oUsuario.getStrCorreo()+"', '"+oUsuario.getStrTelefono()+"',"
//                    +"'"+oUsuario.getStrCelular1()+"', '"+oUsuario.getStrContrasena()+"', "+periodo+", '"+oUsuario.getStrTipo()+"', ' ');";
//                   ad.EjecutarUpdate(sql);
//                   System.out.println(2);
//                   break;
//               case "Administrativo":
//                   sql = "INSERT INTO tbladministrativo(\n"
//                    + "id, nombres, apellidos, cedula, edad, correo, telefono, celular1, contrasena, id_periodo, tipo, cargo)\n"
//                    + "VALUES ('"+aux+"', '"+oUsuario.getStrNombre()+"', '"+oUsuario.getStrApellido()+"', '"+oUsuario.getStrCedula()+"',"
//                    +" "+oUsuario.getIntEdad()+", '"+oUsuario.getStrCorreo()+"', '"+oUsuario.getStrTelefono()+"',"
//                    +"'"+oUsuario.getStrCelular1()+"', '"+oUsuario.getStrContrasena()+"', "+periodo+", '"+oUsuario.getStrTipo()+"', ' ');";
//                   ad.EjecutarUpdate(sql);
//                   System.out.println(3);
//                   break;
//           }
//           return result.toString();
//       }catch(Exception e){
//           Logger log = Logger.getLogger(this.getClass().getName());
//            log.severe(e.getMessage());
//            ac.RollbackTran();
//            return("Error");
//       }
//   }

//MÉTODO PARA GUARDAR EL TOKEN DE UN USUARIO------------------------------------
//    public String getSQLToToken(String json){
//        try{
//            System.out.println("Aqui el json: "+json);
//            String result = "";
//            String auxTipo = "";
//            Integer aux = -1;
//            if(!json.equals("")){
//                System.out.println("Si");
//                JSONParser parser = new JSONParser();
//                Object obj = parser.parse(json);
//                JSONObject oJson = (JSONObject) obj;
//                String strCedula = (String)oJson.get("strCedula");
//                String strToken = (String)oJson.get("strToken");
//                if(!strCedula.equals("") && !strToken.equals("") && ac.Connectar()==2){
//                    String sql = "UPDATE tblusuario SET tokenmovil='"+strToken+"' WHERE (cedula ='"+strCedula+"')";
//                    System.out.println(sql);
//                    ac.BeginTran();
//                    Integer aux2 = ac.EjecutarUpdate(sql);
//                    
//                    if(aux2 == 1){          
//                        sql = "SELECT tipo FROM tblusuario WHERE cedula ='"+strCedula+"'";
//                        ac.EjecutarSQL(sql);
//                        ResultSet rs = ac.getRs();
//                        if(rs.next()){
//                            auxTipo = rs.getString("tipo");
//                        }
//                        ac.CommitTran();
//                        ac.Desconectar();
//                        if(!auxTipo.equals("")){//Segun el tipo de usuario se actualiza en su tabla correspondiente
//                            switch(auxTipo){
//                                case "Representante":
//                                    result = "UPDATE tblrepresentante set tokenmovil='"+strToken+"' WHERE (cedula ='"+strCedula+"');";
//                                    break;
//                                case "Docente":
//                                    result = "UPDATE tbldocente set tokenmovil='"+strToken+"' WHERE (cedula ='"+strCedula+"');";
//                                    break;
//                                default : result = "";
//                            }
//                        }
//                    }  
//                }
//            }
//            return result;
//        }
//            catch(Exception e){
//            Logger log = Logger.getLogger(this.getClass().getName());
//            log.severe(e.getMessage());
//            ac.RollbackTran();
//            return "";
//        }
//    }
   
    //Método para devolver el tipo de un usuario mediante cédula----------------
    public String tipoUsuario(String cedula){
        String sql = "select id from tblusuario where cedula ='"+cedula+"' ";
        String resultado = "";
        try{
            if(ac.Connectar() ==2){
               ac.BeginTran();
                ac.EjecutarSQL(sql);
                ResultSet rs = ac.getRs();
                if(rs.next()){
                    resultado = rs.getString("id");
                }
                System.out.println(resultado); 
                
            }
            return "";
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ac.RollbackTran();
            return resultado;
            
        }  
    }


//MÉTODO PARA EL INGRESO DE UN NUEVO PERIODO ACADÉMICO--------------------------    
    public Integer getIdPeriodo(String Nperiodo, AccesoDatos ad) {
        Integer result = 0;
        if(!Nperiodo.equals("")){
            oPeriodo = oGson.fromJson(Nperiodo, periodo.class);
            if(oPeriodo.getIntIdPeriodo() != 0){
                result = oPeriodo.getIntIdPeriodo();
            } else {
//                if(ac.Connectar() == 2){
//                    ac.BeginTran();
                    ad.EjecutarUpdate("INSERT INTO tblperiodoacademico (periodo_aca) VALUES ('"+oPeriodo.getStrPeriodoAcademico()+"')");
                    ad.EjecutarSQL("SELECT id_periodo FROM tblperiodoacademico ORDER BY id_periodo DESC LIMIT 1" );
                    ResultSet rs = ad.getRs();
                    try {
                        if(rs.next()){
                           result = rs.getInt("id_periodo");
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger(sesionAD.class.getName()).log(Level.SEVERE, null, ex);
                    }
//                    ac.CommitTran();
//                    ac.Desconectar();
                //}
            }
        }
        return result;
    }


}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ggm.accesodatos;

import com.google.gson.Gson;
import ggm.comun.docente;
import ggm.comun.usuario;
import ggm.comun.actividad;
import ggm.comun.clase;
import ggm.comun.estudiante;
import ggm.comun.materia;
import ggm.conexion.AccesoDatos;
import java.util.logging.Logger;

/**
 *
 * @author Cristhian
 */
public class docenteAD {
    private usuario Usuario;
    private docente oDocente;
    private Gson oGson;
    private AccesoDatos ac;
    private String resultado; 
    private materia objetoMateria;
    
    public docenteAD(){
        Usuario = new usuario();
        oDocente = new docente();
        oGson = new Gson();
        ac = new AccesoDatos();
        objetoMateria = new materia();
        resultado = "";
    }
    
//ACCESO A DATOS PARA REGISTRAR UN DOCENTE--------------------------------------
    public String getSQLToInsertarDocente(docente objeto){
        if(objeto != null){
            resultado = "INSERT INTO tbldocente(\n"
                    + "id, nombres, apellidos, cedula, edad, correo, telefono, celular1, contrasena, id_periodo, tipo, titulo1_docente)\n"
                    + "VALUES ("+objeto.getIntIdUsuario()+", '"+objeto.getStrNombre()+"', '"+objeto.getStrApellido()+"', '"+objeto.getStrCedula()+"',"
                    +" "+objeto.getIntEdad()+", '"+objeto.getStrCorreo()+"', '"+objeto.getStrTelefono()+"',"
                    +"'"+objeto.getStrCelular1()+"', '"+objeto.getStrContrasena()+"', "+objeto.getIntPeriodo()+", '"+objeto.getStrTipo()+"', '"+objeto.getStrTitulo1()+"');";
        }
        return resultado;
    }
 
//ACCESO A DATOS PARA BUSCAR UN DOCENTE-----------------------------------------
    public String getSQLToBuscarDocente(docente objeto){
        if(objeto != null){
            resultado = "select *\n"
                    + "from tbldocente\n"
                    + "where cedula = '"+ objeto.getStrCedula()+"'\n";
        }
        System.out.println(resultado);
        return resultado;
    }
    
    //ACCESO A DATOS PARA BUSCAR UN DOCENTE MEDIANTE LA CLASE A LA QUE PERTENECE
    public String getSQLToBuscarDocenteForClase(clase objeto){
        if(objeto != null){
            resultado = "select td.id, td.nombres, td.apellidos, td.tokenmovil from tbldocente as td "
            + "inner join tblclase as tc on td.id = tc.id where tc.id = "+objeto.getIntIdDocente()+" group by td.id";
        }
        System.out.println(resultado);
        return resultado;
    }
    
//ACCESO A DATOS PARA MODIFICAR LA INFORMACION DE UN DOCENTE--------------------
    public String getSQLToModificarDocente(docente objeto){
        if(objeto != null){
               resultado = "UPDATE tbldocente SET nombres='"+objeto.getStrNombre()+"', apellidos= '"+objeto.getStrApellido()+"', edad='"
                +objeto.getIntEdad()+"', correo='"+objeto.getStrCorreo()+"', telefono='"           
                +objeto.getStrTelefono()+"', celular1='"+objeto.getStrCelular1()+"', titulo1_docente='"+objeto.getStrTitulo1()+"', titulo2_docente='"
                +objeto.getStrTitulo2()+"', titulo3_docente='"+objeto.getStrTitulo3()+"' WHERE (cedula = '"+objeto.getStrCedula()+"');";
        }
        System.out.println(resultado);
        return resultado;
    }
//Modifica la información de la tabla docente-----------------------------------
    public Integer getSQLToUpdate2(String json) {
        try{
        oDocente = oGson.fromJson(json, docente.class);
        Integer aux=0;
        String sql = "UPDATE tblusuario SET nombres='"+oDocente.getStrNombre()+"', apellidos= '"+oDocente.getStrApellido()+"', edad="
                +oDocente.getIntEdad()+", correo='"+oDocente.getStrCorreo()+"', telefono='"+oDocente.getStrTelefono()+"', celular1='"           
                +oDocente.getStrCelular1()+"' WHERE (cedula = '"+oDocente.getStrCedula()+"');";
        if(ac.Connectar()==2){
           ac.BeginTran();
            aux = ac.EjecutarUpdate(sql);
                ac.CommitTran();
                ac.Desconectar();
                return aux;
        }else return aux;
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ac.RollbackTran();
            return (2);
            }
        }
    
//ACCESO A DATOS PARA EL LISTADO DE PARALELOS DE UN DOCENTE DADA LA MATERIA-----
    public String getSQLToSelectParalelosForDocenteMateria(clase objeto) {
        if(objeto != null){
            resultado = "select tp.id_paralelo, tp.id_periodo, tp.paralelo, tp.descripcion_par "
            + "from tblparalelo as tp inner join tblclase as tc on tp.id_paralelo = tc.id_paralelo "
            + "where tc.id = "+objeto.getIntIdDocente()+" and tc.id_materia = "+objeto.getIntIdMateria()+"";
        }
        System.out.println(resultado);
        return resultado;
    }

//MÉTODO PARA EL LISTADO DE ACTIVIDADES POR FECHA DE PLAZO DE UN DOCENTE--------    
//    public String getSQLToSelectActividadesFecha(String json) {
//        if(!json.equals("")){
//            oDocente = oGson.fromJson(json, docente.class);
//            resultado = "select ta.id_actividad, ta.id_materia, ta.nombre_act, ta.f_realizado, ta.f_plazo, ta.descripcion_act, ta.tipo_act, ta.destinatarios "
//                    + "from tbldocente as td inner join clase as tc on td.id = tc.id inner join tblmateria as tm on tc.id_materia = tm.id_materia "
//                    + "inner join tblactividad as ta on tm.id_materia = ta.id_materia where td.cedula='"+oDocente.getStrCedula()+"' group by ta.id_actividad order by ta.f_plazo asc";
//        }
//        System.out.println(resultado);
//        return resultado;
//    }
    
//MÉTODO PARA CONOCER LOS DESTINATARIOS DE UNA ACTIVIDAD------------------------
    public String getSQLToSelectDestinatariosActividades(actividad aux) {
        if(aux != null){
            resultado = "select tp.paralelo from tblactividad as ta inner join recibe as tr on ta.id_actividad = tr.id_actividad "
                    + "inner join tblestudiante as te on tr.id_estudiante = te.id_estudiante inner join tblparalelo as tp on te.id_paralelo = tp.id_paralelo "
                    + "where ta.id_Actividad = "+aux.getIntIdActividad()+" group by tp.paralelo";
        }
        System.out.println(resultado);
        return resultado;
    }
    
//ACCESO A DATOS PARA ASIGNAR EL TOKEN A UN DOCENTE-------------------------
    public String getSQLToAsignarTokenDocente(docente objeto){
        if(objeto != null){
            resultado = "UPDATE tbldocente SET tokenmovil='"+objeto.getStrToken()+"' WHERE (cedula ='"+objeto.getStrCedula()+"')";
        }
        System.out.println(resultado);
        return resultado;
    }

    public String getSQLToListarClasesDocente(docente objeto) {
        if(objeto != null){
            resultado = "select tc.idclase, tc.id_paralelo as idparaleloclase, tc.id_materia as idmateriaclase, tc.id, tp.id_paralelo, "
            + "tp.paralelo, tm.id_materia, tm.nombre_mat from tblclase as tc inner join tbldocente as td on tc.id = td.id "
            + "inner join tblparalelo as tp on tc.id_paralelo = tp.id_paralelo inner join tblmateria as tm on tc.id_materia = tm.id_materia "
            + "where tc.id = "+objeto.getIntIdUsuario()+"";
        }
        return resultado;
    }
    
    
//ACCESO A DATOS PARA LISTAR LOS DOCENTES DE UN ESTUDIANTE----------------------
    public String getSQLToListarDocentesDeEstudiante(estudiante objeto) {
        if(objeto != null){
            resultado = "select td.id, td.nombres, td.apellidos, td.cedula, tma.nombre_mat from tbldocente as td "
            + "inner join tblclase as tc on td.id = tc.id inner join tblmateria as tma on tc.id_materia = tma.id_materia inner join tblcursa as tcu on tc.idclase = tcu.idclase "
            + "inner join tblmatricula as tm on tcu.idmatricula = tm.idmatricula inner join tblestudiante as te "
            + "on tm.id_estudiante = te.id_estudiante where te.id_estudiante = "+objeto.getIntIdEstudiante()+"";
        }
        System.out.println(resultado);
        return resultado;
    }  
    
}


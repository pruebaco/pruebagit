/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ggm.accesodatos;

import com.google.gson.Gson;
import ggm.comun.cursa;
import ggm.conexion.AccesoDatos;

/**
 *
 * @author Cristhian
 */
public class cursaAD {
    private Gson objetoGson;
    private AccesoDatos ac;
    private String resultado;
    
    public cursaAD(){
        this.objetoGson = new Gson();
        this.resultado = ""; 
                
    }
    
//ACCESO A DATOS PARA BUSCAR EL ID DE CURSA DADO CLASE Y MATRICULA--------------
    public String getSQLToBuscarIdCursa(cursa objeto){
        if(objeto != null){
            resultado = "select tc.idcursa from tblcursa as tc where "
            + "tc.idmatricula = "+objeto.getIntIdMatricula()+" and tc.idclase = "+objeto.getIntIdClase()+"";
        }
        System.out.println(resultado);
        return resultado;
    }
    
//ACCESO A DATOS PARA INGRESAR UN NUEVO REGISTRO EN LA TABLA CURSA--------------
    public String getSQLToIngresarCursa(cursa objeto){
        if(objeto != null){
            resultado = "insert into tblcursa (idmatricula, idclase) values "
            + "("+objeto.getIntIdMatricula()+", "+objeto.getIntIdClase()+")";
        }
        System.out.println(resultado);
        return resultado;
    }
}

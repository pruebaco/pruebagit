/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ggm.accesodatos;

import com.google.gson.Gson;
import ggm.comun.docente;
import ggm.comun.notificacion;
import ggm.comun.representante;
import ggm.conexion.AccesoDatos;
import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author Cristhian
 */
public class notificacionAD {
    private Gson objetoGson;
    private AccesoDatos ad;
    private String resultado;
    private Date dtFecha;
    
    public notificacionAD(){
        objetoGson = new Gson();
        ad = new AccesoDatos();
        resultado = "";
        dtFecha = new Date();
    }
    
//ACCESO A DATOS PARA INGRESAR UNA NOTIFICACION---------------------------------
    public String getSQLToIngresarNotificacion(notificacion objeto){
        if(objeto != null){
            resultado = "insert into notificacion (tbl_id, id, nombre_not, cod_activ, fecha_envio) "
            + "values ("+objeto.getIntIdDocente()+", "+objeto.getIntIdRepresentante()+", "
            + "'"+objeto.getStrNombreNotificacion()+"', "+objeto.getIntCodigoActividad()+", '"+objeto.getStrFechaEnvio()+"')";
        }
        System.out.println(resultado);
        return resultado;
    }
    
//ACCESO A DATOS PARA MODIFICAR UNA NOTIFICACION--------------------------------
    public String getSQLToModificarNotificacion(notificacion objeto){
        if(objeto != null){
            resultado = "update notificacion set nombre_not='"+objeto.getStrNombreNotificacion()+"', respuesta='"+objeto.getStrRespuesta()+"', "
            + "fecha_respuesta='"+new Timestamp(dtFecha.getTime()).toString()+"', "
            + "estado_docente="+objeto.getBooEstadoDocente()+", estado_representante="+objeto.getBooEstadoRepresentante()+" "
            + "where id_notificacion="+objeto.getIntIdNotificacion()+"";
        }
        return resultado;           
    }
    
//ACCESO A DATOS PARA BUSCAR UNA NOTIFICACION-----------------------------------
    public String getSQLToBuscarNotificacion(notificacion objeto){
        if(objeto != null){
            resultado = "select * from tblnotificacion where id_notificacion="+objeto.getIntIdNotificacion()+"";
        }
        return resultado;
    }
    
    //MÉTODO PARA LISTAR LAS NOTIFICACIONES DE UN REPRESENTANTE---------------------
    public String getSQLToListNotificacionesRepresentantes(representante objeto){
        if(objeto != null){
            resultado = "select tn.id_notificacion, tn.id, tn.tbl_id, tn.nombre_not, tn.respuesta, tn.fecha_envio, tn.fecha_respuesta, "
            + "tn.estado_docente, tn.estado_representante, tr.nombres as nombreRepresentante, tr.tokenmovil as tokenrepresentante, te.nombre_est, tea.calificacion, ta.id_actividad as idactividad, ta.nombre_act, ta.f_realizado, "
            + "ta.f_plazo, ta.descripcion_act, ta.tipo_act, tm.nombre_mat, tp.paralelo, td.nombres, td.tokenmovil as tokendocente from notificacion as tn inner join "
            + "tblrepresentante as tr on tn.id = tr.id inner join tblestudiante as te on tr.id = te.id inner join estudianteactividad as tea "
            + "on te.id_estudiante = tea.id_estudiante inner join tblactividad as ta on tea.id_actividad = ta.id_actividad inner join tblclase as tc "
            + "on ta.idclase = tc.idclase inner join tbldocente as td on tc.id = td.id inner join tblparalelo as tp on tc.id_paralelo = tp.id_paralelo "
            + "inner join tblmateria as tm on tc.id_materia = tm.id_materia where tn.cod_activ = ta.id_actividad and tr.id = "+objeto.getIntIdUsuario()+" order by tn.fecha_respuesta desc";
        System.out.println(resultado);
        }

        return resultado;
    }

//MÉTODO PARA LISTAR LAS NOTIFICACIONES DE UN DOCENTE---------------------------    
    public String getSQLToListNotificacionesDocente(docente objeto) {
        if(objeto != null){
            resultado = "select tn.id_notificacion, tn.id, tn.tbl_id, tn.nombre_not, tn.respuesta, tn.fecha_envio, tn.fecha_respuesta, "
            + "tn.estado_docente, tn.estado_representante, tr.nombres as nombreRepresentante, tr.tokenmovil as tokenrepresentante, te.nombre_est, tea.calificacion, ta.id_actividad as idactividad, ta.nombre_act, ta.f_realizado, "
            + "ta.f_plazo, ta.descripcion_act, ta.tipo_act, tm.nombre_mat, tp.paralelo, td.nombres, td.tokenmovil as tokendocente from notificacion as tn inner join "
            + "tblrepresentante as tr on tn.id = tr.id inner join tblestudiante as te on tr.id = te.id inner join estudianteactividad as tea "
            + "on te.id_estudiante = tea.id_estudiante inner join tblactividad as ta on tea.id_actividad = ta.id_actividad inner join tblclase as tc "
            + "on ta.idclase = tc.idclase inner join tbldocente as td on tc.id = td.id inner join tblparalelo as tp on tc.id_paralelo = tp.id_paralelo "
            + "inner join tblmateria as tm on tc.id_materia = tm.id_materia where tn.cod_activ = ta.id_actividad and td.id = "+objeto.getIntIdUsuario()+" order by tn.fecha_respuesta desc";
        }
        System.out.println(resultado);
        return resultado;
    }
    
}



/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ggm.accesodatos;

import com.google.gson.Gson;
import ggm.comun.actividad;
import ggm.comun.calificacion;
import ggm.comun.clase;
import ggm.comun.docente;
import ggm.comun.estudiante;
import ggm.comun.representante;
import ggm.conexion.AccesoDatos;
import java.sql.Timestamp;
import java.util.Date;

/**
 *
 * @author Cristhian
 */
public class actividadAD {
    private actividad oActividad;
    private Gson oGson;
    private AccesoDatos ad;
    private String resultado;
    
    public actividadAD(){
        oGson = new Gson();
        ad = new AccesoDatos();
        oActividad = new actividad();
        resultado = "";
    }
    
    public String listarActividadesFechaU(String json) {
        
        return "";
    }
    
//MÉTODO PARA INGRESAR UNA ACITIVIDAD POR PARTE DE UN DOCENTE-------------------    
    public String getSQLToIngresarActividad(actividad objeto) {
        Date objetoDate = new Date();
        if(objeto != null){
            resultado = "INSERT INTO tblactividad (idclase, nombre_act, f_realizado, f_plazo, descripcion_act, tipo_act) "
                    + "VALUES ("+objeto.getIntIdClase()+", '"+objeto.getStrNombre()+"', '"+new Timestamp(objetoDate.getTime())+"', "
                    + "'"+objeto.getStrFechaPlazo()+"',  '"+objeto.getStrDescripcion()+"', '"+objeto.getStrTipo()+"')";
        }
        System.out.println(resultado);
        return resultado;
    }
    
//MÉTODO PARA OBTENER LA ÚLTIMA ACTIVIDAD AGREGADA------------------------------
        public String getSQLToSelectLastIdActivity(){
            resultado = "SELECT id_actividad FROM tblactividad ORDER BY id_actividad DESC LIMIT 1";
            System.out.println(resultado);
            return resultado;
        }
        
//MÉTODO PARA ASIGNAR LA ACTIVIDAD A ESTUDIANTES--------------------------------    
    public String getSQLToIngresarEstudianteActividad(Integer idActiv, Integer idEstud){
        if(idEstud != 0){
            resultado = "INSERT INTO estudianteactividad (id_actividad, id_estudiante, calificacion) VALUES ("+idActiv+", "+idEstud+", "+0+")";
        }
        System.out.println(resultado);
        return resultado;
    }
    
//MÉTODO PARA MODIFICAR UNA ACTIVIDAD-------------------------------------------
    public String getSQLToModificarActividad(actividad objeto) {
        if(objeto != null){
            resultado = "UPDATE tblactividad SET nombre_act = '"+objeto.getStrNombre()+"', "
                    + "f_plazo = '"+objeto.getStrFechaPlazo()+"', "
                    + "descripcion_act = '"+objeto.getStrDescripcion()+"', tipo_act = '"+objeto.getStrTipo()+"' "
                    + "WHERE id_actividad = "+objeto.getIntIdActividad()+"";
        }
        System.out.println(resultado);
        return resultado;
    }
    
   

    public String getSQLToSelectDestinatariosActividades(Integer idActividad) {
        if(idActividad != null){
            resultado = "select tp.paralelo from tblactividad as ta inner join recibe as tr on ta.id_actividad = tr.id_actividad "
                    + "inner join tblestudiante as te on tr.id_estudiante = te.id_estudiante inner join tblparalelo as tp on te.id_paralelo = tp.id_paralelo "
                    + "where ta.id_Actividad = "+idActividad+" group by tp.paralelo";
        }
        System.out.println(resultado);
        
        return resultado;
    }

    String getSQLToSetDestinatarios(String destinatariosActividades, int SQLaux3) {
        if(destinatariosActividades != null && SQLaux3 != 0){
            resultado = "UPDATE tblactividad SET destinatarios = '"+destinatariosActividades+"' WHERE id_actividad = "+SQLaux3+"";
        }
        return resultado;
    }
    
//ACCESO A DATOS PARA LISTAR ACTIVIDADES DE UN DOCENTE POR FECHA PLAZO----------
    public String ListActividadesFechaDocente(docente objeto){
        if(objeto != null){
            resultado = "select ta.id_Actividad, ta.idclase, ta.nombre_act, ta.f_realizado, "
            + "ta.f_plazo, ta.descripcion_act, ta.tipo_act from tbldocente as td "
            + "inner join tblclase as tc on td.id = tc.id inner join tblactividad as ta "
            + "on tc.idclase = ta.idclase where td.cedula = '"+objeto.getStrCedula()+"' order by ta.f_plazo asc";
        }
        return resultado;
    }

//ACCESO A DATOS PARA INGRESAR LA CALIFICACION DE UNA ACTIVIDAD-----------------    
    public String getSQLToModificarCalificacionActividad(calificacion objeto) {
        if(objeto != null){
            resultado = "update estudianteactividad set calificacion="+objeto.getIntCalificacion()+"";
        }
        return resultado;
    }
    
//ACCESO A DATOS PARA BUSCAR UNA ACTIVIDAD--------------------------------------
    public String getSQLToBuscarActividad(actividad objeto){
        if(objeto != null){
            resultado = "select * from tblactividad as ta where ta.id_actividad ="+objeto.getIntIdActividad()+"";
        }
        System.err.println(resultado);
        return resultado;
    }

//
    public String getSQLToListarActividadesEstudiantesRepresentante(representante objeto) {
        if(objeto != null){
            resultado = "select te.nombre_est, ta.id_actividad, ta.idclase, ta.nombre_act, "
            + "ta.f_realizado, ta.f_plazo, ta.descripcion_act, ta.tipo_act, tea.calificacion "
            + "from tblrepresentante as tr inner join tblestudiante as te on tr.id = te.id "
            + "inner join estudianteactividad as tea on te.id_estudiante = tea.id_estudiante "
            + "inner join tblactividad as ta on tea.id_actividad = ta.id_actividad where tr.cedula='"+objeto.getStrCedula()+"'";
        }
        System.out.println(resultado);
        return resultado;
    }
    
//ACCESO A DATOS PARA LISTAR LAS ACTIVIDADES Y NOTAS DE UN ESTUDIANTE-----------
    public String getSQLToListarActividadesNotasEstudiante(estudiante objeto){
        if(objeto!=null){
            resultado = "SELECT ta.id_actividad, ta.idclase, ta.nombre_act, ta.f_realizado, ta.f_plazo, "
            + "ta.descripcion_act, ta.tipo_act, ea.calificacion FROM tblestudiante as te "
            + "inner join estudianteactividad as ea on te.id_estudiante = ea.id_estudiante "
            + "inner join tblactividad as ta on ea.id_actividad = ta.id_actividad "
            + "where te.cedula_est='"+objeto.getStrCedula()+"' order by ta.f_plazo desc";
        }
        System.out.println(resultado);
        return resultado;
    }
    
//ACCESO A DATOS PARA LISTAR LAS ACTIVIDADES DE UNA CLASE-----------------------
    public String getSQLToListarActividadesClase(clase objeto){
        if(objeto!=null){
            resultado = "select ta.id_actividad, ta.idclase, ta.nombre_act, ta.f_realizado, "
            + "ta.f_plazo, ta.descripcion_act, ta.tipo_act from tblactividad as ta inner join "
            + "tblclase as tc on ta.idclase = tc.idclase where tc.idclase = "+objeto.getIntIdClase()+"";         
        }
        return resultado;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ggm.accesodatos;

import com.google.gson.Gson;
import ggm.comun.clase;
import ggm.comun.docente;
import ggm.comun.paralelo;

/**
 *
 * @author Cristhian
 */
public class paraleloAD {
    private String resultado;
    private paralelo objetoParalelo;
    private docente objetoDocente;
    private Gson objetoGson;
    
    public paraleloAD(){
        resultado = "";
        objetoParalelo = new paralelo();
        objetoDocente = new docente();
        objetoGson = new Gson();
    }
    
//ACCESO A DATOS PARA EL LISTADO DE PARALELOS DE LA INSTITUCIÓN-----------------
    public String getSQLToListarTodosParalelos() {
        resultado = "select * from tblparalelo";
        return resultado;
    }
    
 //ACCESO A DATOS PARA BUSCAR PARALELO MEDIANTE SU CLASE------------------------
    public String getSQLToBuscarParaleloForClase(clase objeto){
        if(objeto != null){
            resultado = "select tp.id_paralelo, tp.paralelo from tblparalelo as "
            + "tp inner join tblclase as tc on tp.id_paralelo = tc.id_paralelo "
            + "where tp.id_paralelo = "+objeto.getIntIdParalelo()+" group by tp.id_paralelo ";
        }
        System.out.println(resultado);
        return resultado;
    }

//ACCESO A DATOS PARA EL INGRESO DE UN NUEVO PARALELO---------------------------
    public String getSQLToRegistrar(paralelo objeto) {
        if(objeto != null){
            resultado = "INSERT INTO tblparalelo (id_periodo, paralelo, descripcion_par) "
                    + "VALUES ("+objeto.getIntIdPeriodo()+", '"+objeto.getStrParalelo()+"', '"+objeto.getStrDescripcion()+"')";
        }
        return resultado;
    }

//ACCESO A DATOS PARA SELECCIONAR EL ULTIMO PARALELO INGRESADO------------------
    public String getSQLToLastParalelo() {
        resultado = "SELECT id_paralelo FROM tblparalelo order by id_paralelo desc limit 1";
        return resultado;
    }
    
//ACCESO A DATOS PARA EL INGRESO EN LA TABLA CLASE------------------------------
    public String getSQLToInsertClase(Integer idUsuario, Integer idParalelo) {
        if(idUsuario != 0 && idParalelo != 0){
            resultado = "INSERT INTO clase (id, id_paralelo) VALUES ("+idUsuario+", "+idParalelo+")";
            System.out.println("Consulta: "+resultado);
        }
        return resultado;
    }
    
//ACCESO A DATOS PARA LISTAR LOS PARALELOS DE UN DOCENTE------------------------
    public String getSQLToListarParalelosDocente(docente objeto){
        if(objeto != null){
            resultado = "select tp.id_paralelo, tp.id_periodo, tp.paralelo, tp.descripcion_par from tbldocente as td inner join tblclase as tc on td.id = tc.id "
                    + "inner join tblparalelo as tp on tc.id_paralelo = tp.id_paralelo where td.cedula = '"+objeto.getStrCedula()+"' group by tp.id_paralelo";
        }
        return resultado;
    }
}

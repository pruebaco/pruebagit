/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ggm.accesodatos;

import com.google.gson.Gson;
import ggm.comun.administrativo;
import ggm.comun.usuario;
import ggm.conexion.AccesoDatos;
import java.util.logging.Logger;


/**
 *
 * @author Cristhian
 */
public class administrativoAD {
    private usuario Usuario;
    private administrativo oAdmin;
    private Gson oGson;
    private AccesoDatos ac;
    private String resultado;
    
    public administrativoAD(){
        Usuario = new usuario();
        oAdmin = new administrativo();
        ac = new AccesoDatos();
        oGson = new Gson();
        resultado = "";
    }

//BUSCAR LA INFORMACION DE UN ADMINISTRATIVO------------------------------------    
    public String getSQLToBuscarAdministrativo(usuario objeto){
        if(objeto != null){
            //Usuario = oGson.fromJson(json, usuario.class);
            resultado = "select *\n"
                    + "from tbladministrativo\n"
                    + "where cedula = '"+ objeto.getStrCedula()+"'\n";
        }
        System.out.println(resultado);
        return resultado;
    }

//MODIFICAR LA INFORMACION DE UN ADMINISTRATIVO----------------------------------    
    public String getSQLToModificarAdministrativo(administrativo objeto){
        if(objeto != null){
               resultado = "UPDATE tbladministrativo SET nombres='"+objeto.getStrNombre()+"', apellidos= '"+objeto.getStrApellido()+"', edad='"
                +objeto.getIntEdad()+"', correo='"+objeto.getStrCorreo()+"', telefono='"           
                +objeto.getStrTelefono()+"', celular1='"+objeto.getStrCelular1()+"', cargo='"+objeto.getStrCargo()+"', titulo1_adm='"
                +objeto.getStrTitulo1()+"', titulo2_adm='"+objeto.getStrTitulo2()+"' WHERE (cedula = '"+objeto.getStrCedula()+"');";
            }
        
        System.out.println(resultado);
        return resultado;
    }
//MODIFICA LA INFORMACION DE LA TABLA USUARIO-----------------------------------
    public Integer getSQLToUpdate2(String json) {
        try{
        oAdmin = oGson.fromJson(json, administrativo.class);
        Integer aux=0;
        String sql = "UPDATE tblusuario SET nombres='"+oAdmin.getStrNombre()+"', apellidos= '"+oAdmin.getStrApellido()+"', edad="
                +oAdmin.getIntEdad()+", correo='"+oAdmin.getStrCorreo()+"', telefono='"+oAdmin.getStrTelefono()+"', celular1='"           
                +oAdmin.getStrCelular1()+"' WHERE (cedula = '"+oAdmin.getStrCedula()+"');";
        System.out.println(sql);
        if(ac.Connectar()==2){
             ac.BeginTran();
            aux = ac.EjecutarUpdate(sql);
            System.out.println("Si");
            ac.CommitTran();
            ac.Desconectar();
            return aux;    
        }else {return aux;}
        }catch(Exception e){
            Logger log = Logger.getLogger(this.getClass().getName());
            log.severe(e.getMessage());
            ac.RollbackTran();
            return (2);
            }
        }

    public String getSQLToIngresarAdministrativo(administrativo objeto) {
        if(objeto != null){
            resultado = "INSERT INTO tbladministrativo(\n"
                    + "id, nombres, apellidos, cedula, edad, correo, telefono, celular1, contrasena, id_periodo, tipo, cargo)\n"
                    + "VALUES ("+objeto.getIntIdUsuario()+", '"+objeto.getStrNombre()+"', '"+objeto.getStrApellido()+"', '"+objeto.getStrCedula()+"',"
                    +" "+objeto.getIntEdad()+", '"+objeto.getStrCorreo()+"', '"+objeto.getStrTelefono()+"',"
                    +"'"+objeto.getStrCelular1()+"', '"+objeto.getStrContrasena()+"', "+objeto.getIntPeriodo()+", '"+objeto.getStrTipo()+"', '"+objeto.getStrCargo()+"');";
        }
        return resultado;
    }

    
}

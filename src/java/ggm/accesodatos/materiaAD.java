/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ggm.accesodatos;

import com.google.gson.Gson;
import ggm.comun.clase;
import ggm.comun.docente;
import ggm.comun.materia;

/**
 *
 * @author Cristhian
 */
public class materiaAD {
    private String resultado;
    private materia objetoMateria;
    private Gson objetoGson;
    
    public materiaAD(){
        resultado = "";
        objetoMateria = new materia();
        objetoGson = new Gson();
    }
    
    public String getSQLToListarMateriasDocente(docente objeto){
        if(objeto != null){
            resultado = "select tm.id_materia, tm.nombre_mat, tm.descripcion_mat from tbldocente as td inner join tblclase as tc on td.id = tc.id"
            + " inner join tblmateria as tm on tc.id_materia = tm.id_materia where td.cedula = '"+objeto.getStrCedula()+"' group by tm.id_materia";
        }
        return resultado;
    }
    
    public String getSQLToBuscarMateriaForClase(clase objeto){
        if(objeto != null){
            resultado = "select tm.id_materia, tm.nombre_mat from tblmateria as tm "
            + "inner join tblclase as tc on tm.id_materia = tc.id_materia where "
            + "tc.id_materia = "+objeto.getIntIdMateria()+" group by tm.id_materia";
        }
        System.out.println(resultado);
        return resultado;
                
    }
    
//ACCESO A DATOS PARA INGRESAR UNA MATERIA--------------------------------------
    public String getSQLToIngresarMateria(materia objeto) {
        if(objeto != null){
            resultado = "insert into tblmateria (nombre_mat, descripcion_mat) "
            + "values ('"+objeto.getStrNombreMAteria()+"', '"+objeto.getStrDescripcionMateria()+"')";
        }
        System.out.println(resultado);
        return resultado;
    }
    
//ACCESO A DATOS PARA OBTENER EL ID DE LA ULTIMA MATERIA INGRESADA--------------
    public String getSQLToBuscarUltimaMateria(){
        resultado = "select id_materia from tblmateria order by id_materia desc limit 1";
        System.out.println(resultado);
        return resultado;
    }

//ACCESO A DATOS PARA LISTAR TODOS LOS PARALELOS CON SUS CLASES-----------------
    public String getSQLToListarParalelosDeDocenteConClase() {
        resultado = "select tc.idclase, tp.id_paralelo, tp.paralelo, tp.descripcion_par "
        + "from tblclase as tc inner join "
        + "tblparalelo as tp on tc.id_paralelo = tp.id_paralelo";
        System.out.println(resultado);
        return resultado;
    }
    
//ACCESO A DATOS PARA LISTAR LAS MATERIAS CONOCIENDO DOCENTE Y PARALELO---------
    public String getSQLToListarMateriasDocenteParalelo(clase objeto){
        if(objeto != null){
            resultado = "select tc.idclase, tm.id_materia, tm.nombre_mat, tm.descripcion_mat "
            + "from tblmateria as tm inner join tblclase as tc on tm.id_materia = tc.id_materia "
            + "where tc.id = "+objeto.getIntIdDocente()+" and tc.id_paralelo = "+objeto.getIntIdParalelo()+"";
        }
        

        
        return resultado;
    }
}

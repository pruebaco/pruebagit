/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ggm.accesodatos;

import com.google.gson.Gson;
import ggm.comun.actividad;
import ggm.comun.docente;
import ggm.comun.representante;
import ggm.conexion.AccesoDatos;

/**
 *
 * @author Cristhian
 */
public class notificacionesAD {
    private docente oDocente;
    private representante oRepresentante;
    private Gson oGson;
    private AccesoDatos ad;
    private actividad oActividad;
    private String resultado;
    
    public notificacionesAD(){
        oDocente = new docente();
        oGson = new Gson();
        ad = new AccesoDatos();
        oRepresentante = new representante();
        oActividad = new actividad();
        resultado = "";
    }

    
//MÉTODO PARA LISTAR LAS NOTIFICACIONES DE UN DOCENTE---------------------------    
    public String getSQLToSelectNotificacionesD(String json) {
        String result = "";
        if(!json.equals("")){
            oDocente = oGson.fromJson(json, docente.class);
            result = "select tn.id_notificacion, tr.nombres, tr.apellidos, tn.tipo_activ, tn.nombre_not, "
                    + "tn.respuesta, tn.cod_activ, tn.fecha_envio, tn.fecha_respuesta from tblrepresentante as tr inner join "
                    + "notificacion as tn on tr.id = tn.id inner join tbldocente as td "
                    + "on tn.id_docente = td.id where td.cedula = '"+oDocente.getStrCedula()+"'";
        }
        System.out.println(result);
        return result;
    }

//MÉTODO PARA LISTAR LAS NOTIFICACIONES DE UN REPRESENTANTE---------------------    
    public String getSQLToSelectNotificacionesR(String json) {
        String result = "";
        if(!json.equals("")){
            oRepresentante = oGson.fromJson(json, representante.class);
            result = "select tn.id_notificacion, td.nombres, td.apellidos, tn.tipo_activ, tn.nombre_not, tn.respuesta,"
                    + " tn.cod_activ, tn.fecha_envio, tn.fecha_respuesta from tblrepresentante as tr"
                    + " inner join notificacion as tn on tr.id = tn.id inner join tbldocente as td"
                    + " on tn.id_docente = td.id where tr.cedula = '"+oRepresentante.getStrCedula()+"'";
        }
        System.out.println(result);
        return result;
        
    }

//MÉTODO PARA INGRESAR UNA ACITIVIDAD POR PARTE DE UN DOCENTE-------------------    
    public String getSQLToInsertActividadD(String json) {
        
        String result = "";
        if(!json.equals("")){
            oActividad = oGson.fromJson(json, actividad.class);
            result = "INSERT INTO tblactividad (id_materia, nombre_act, f_realizado, f_plazo, descripcion_act, tipo_act) "
                    + "VALUES ("+oActividad.getIntIdClase()+", '"+oActividad.getStrNombre()+"', '"+oActividad.getStrFechaRealizado()+"',"
                    + "'"+oActividad.getStrFechaPlazo()+"', '"+oActividad.getStrDescripcion()+"', '"+oActividad.getStrTipo()+"')";
        }
        System.out.println(result);
        return result;
    }

//MÉTODO PARA OBTENER LA ÚLTIMA ACTIVIDAD AGREGADA------------------------------
        public String getSQLToSelectLastActivity(){
            String result = "";
            result = "SELECT id_actividad FROM tblactividad ORDER BY id_actividad DESC LIMIT 1";
            System.out.println(result);
            return result;
        }
    
//MÉTODO PARA ASIGNAR LA ACTIVIDAD A ESTUDIANTES--------------------------------    
    public String getSQLToAsignarActividadAE(Integer idActiv, Integer idEstud){
        String result = "";
        if(idEstud!=0){
            result = "INSERT INTO recibe (id_actividad, id_estudiante) VALUES ("+idActiv+", "+idEstud+")";
        }
        //System.out.println(result);
        return result;
    }

//MÉTODO PARA RECUPERAR EL TOKEN DEL REPRESENTANTE DE UN ESTUDIANTE-------------    
    public String getSQLToSelectTokenRepresentantes(int idEstud) {
        String result = "";
        if(idEstud != 0){
            result = "select tr.tokenmovil from tblestudiante as te inner join tblrepresentante as tr on te.id = tr.id where te.id_estudiante = "+idEstud+"";
            System.out.println(result);
        }
        return result;
    }
    

    
    

}

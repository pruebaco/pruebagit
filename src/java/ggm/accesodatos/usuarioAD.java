/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ggm.accesodatos;

import com.google.gson.Gson;
import ggm.comun.administrativo;
import ggm.comun.usuario;

/**
 *
 * @author Cristhian
 */
public class usuarioAD {
    private usuario objetoUsuario;
    private administrativo objetoAdministrativo;
    private Gson objetoGson;
    private String resultado;
    
    
    public usuarioAD(){
        objetoUsuario = new usuario();
        objetoAdministrativo = new administrativo();
        objetoGson = new Gson();
        resultado = "";
    }
    
    public String getSQLToModificarUsuario(usuario objeto){
        if(objeto != null){
            resultado = "UPDATE tblusuario SET nombres='"+objeto.getStrNombre()+"', apellidos= '"+objeto.getStrApellido()+"', edad="
                +objeto.getIntEdad()+", correo='"+objeto.getStrCorreo()+"', telefono='"+objeto.getStrTelefono()+"', celular1='"           
                +objeto.getStrCelular1()+"' WHERE (cedula = '"+objeto.getStrCedula()+"');";
        }
        System.out.println(resultado);
        return resultado;
    }

    public String getSQLToIngresarUsuario(usuario objeto) {
        if(objeto != null){
            resultado = "INSERT INTO tblusuario(\n"
                    + "nombres, apellidos, cedula, edad, correo, telefono, celular1, contrasena, id_periodo, tipo)\n"
                    + "VALUES ('"+objeto.getStrNombre()+"', '"+objeto.getStrApellido()+"', '"+objeto.getStrCedula()+"',"
                    +" "+objeto.getIntEdad()+", '"+objeto.getStrCorreo()+"', '"+objeto.getStrTelefono()+"',"
                    +"'"+objeto.getStrCelular1()+"', '"+objeto.getStrContrasena()+"', "+objeto.getIntPeriodo()+", '"+objeto.getStrTipo()+"');";
        }
        return resultado;
    }
    
    public String getSQLToBuscarIdUsuario(usuario objeto){
        if(objeto != null){
            resultado = "select id from tblusuario where cedula ='"+objeto.getStrCedula()+"'";
        }
        return resultado;
    }
    
//ACCESO A DATOS PARA ASIGNAR TOKEN A UN USUARIO--------------------------------    
    public String getSQLToAsignarTokenUsuario(usuario objeto){
        if(objeto != null){
            resultado = "UPDATE tblusuario SET tokenmovil='"+objeto.getStrToken()+"' WHERE (cedula ='"+objeto.getStrCedula()+"')";
        }
        return resultado;
    }
    
//ACCESO A DATOS PARA OBETENER EL TIPO DE UN USUARIO----------------------------
    public String getSQLToTipoUsuario(usuario objeto){
        if(objeto != null){
            resultado = "SELECT tipo FROM tblusuario WHERE cedula ='"+objeto.getStrCedula()+"'";
            System.out.println(resultado);
        }
        return resultado;
    }
    
}

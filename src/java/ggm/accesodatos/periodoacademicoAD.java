/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ggm.accesodatos;

import com.google.gson.Gson;
import ggm.comun.paralelo;
import ggm.comun.periodo;
import ggm.conexion.AccesoDatos;

/**
 *
 * @author Cristhian
 */
public class periodoacademicoAD {
    private Gson objetoGson;
    private paralelo objetoParalelo;
    private AccesoDatos ad;
    private String resultado;
    
    public periodoacademicoAD(){
        objetoGson = new Gson();
        objetoParalelo = new paralelo();
        ad = new AccesoDatos();
        resultado = "";
    }
    
//ACCESO A DATOS PARA INGRESAR UN NUEVO PERIODO ACADÉMICO-----------------------    
    public String getSQLToIngresarPeriodo(periodo objeto) {
        if(objeto != null){
            resultado = "INSERT INTO tblperiodoacademico (periodo_aca) "
                    + "VALUES ('"+objeto.getStrPeriodoAcademico()+"')";
        }
        return resultado;
    }
    
//ACCESO A DATOS PARA SELECCIONAR EL ÚLTIMO PERIODO REGISTRADO------------------
public String getSQLToBuscarUltimoPeriodo(){
    resultado = "SELECT id_periodo FROM tblperiodoacademico ORDER BY id_periodo DESC LIMIT 1";
    return resultado;
}   

    public String getSQLToListarPeriodos() {
        return "SELECT * FROM tblperiodoacademico ORDER BY periodo_aca DESC LIMIT 5";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ggm.accesodatos;

import com.google.gson.Gson;
import ggm.comun.cursa;
import ggm.comun.evaluacion;
import ggm.comun.parcial;
import ggm.conexion.AccesoDatos;

/**
 *
 * @author Cristhian
 */
public class evaluacionAD {
    private evaluacion oNotasParcial;
    private Gson oGson;
    private AccesoDatos ad;
    private String resultado;
    
    public evaluacionAD(){
        oNotasParcial = new evaluacion();
        oGson = new Gson();
        ad = new AccesoDatos();
        resultado = "";
    }

//ACCESO A DATOS PARA EL REGISTRO DE NOTAS DE UN ESTUDIANTE---------------------
    public String getSQLToIngresarEvaluacion(cursa objetoCursa, parcial objetoParcial, evaluacion objetoEvaluacion) {
        if(objetoCursa != null && objetoParcial != null && objetoEvaluacion != null){
            resultado = "insert into tblevaluacion (idparcial, idcursa, nota1, nota2, nota3, nota4, descripcionnota) "
                    + "values ("+objetoParcial.getIntIdParcial()+", "+objetoCursa.getIntIdCursa()+", "
                    + ""+objetoEvaluacion.getIntNota1()+", "+objetoEvaluacion.getIntNota2()+", "+objetoEvaluacion.getIntNota3()+", "+objetoEvaluacion.getIntNota4()+", "
                    + "'"+objetoEvaluacion.getStrDescripcion()+"')";
        }
        System.out.println(resultado);
        return resultado;
    }   

//ACCESO A DATOS PARA LA MODIFICACION DE UNA EVALUACION-------------------------    
    public String getSQLToModificarEvaluacion(evaluacion objetoEvaluacion) {
        if(objetoEvaluacion != null){
            resultado = "update tblevaluacion set nota1="+objetoEvaluacion.getIntNota1()+", "
            + "nota2="+objetoEvaluacion.getIntNota2()+", nota3="+objetoEvaluacion.getIntNota3()+", "
            + "nota4="+objetoEvaluacion.getIntNota4()+" where(idnota = "+objetoEvaluacion.getIntIdEvaluacion()+")";
        }
        System.out.println(resultado);
        return resultado;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ggm.accesodatos;

import com.google.gson.Gson;
import ggm.comun.clase;

/**
 *
 * @author Cristhian
 */
public class claseAD {
    private String resultado;
    private Gson objetoGson;
    private clase objetoClase;
    
    public claseAD(){
        this.resultado = "";
        this.objetoGson = new Gson();
        this.objetoClase = new clase();
    }
    
//ACCESO A DATOS PARA INGRESAR UNA NUEVA CLASE COMPLETA--------------------------    
    public String getSQLToIngresarClase(clase objeto){
        if(objeto != null){
            resultado = "insert into tblclase (id_paralelo, id_materia, id) values "
                    + "("+objeto.getIntIdParalelo()+", "+objeto.getIntIdMateria()+", "
                    + ""+objeto.getIntIdDocente()+")";
        }
        System.out.println(resultado);
        return resultado;
    }
    
    public String getSQLToBuscarIdClase(clase objeto){
        if(objeto != null){
            resultado = "select idclase from tblclase where id_paralelo = "+objeto.getIntIdParalelo()+" "
                    + "and id_materia = "+objeto.getIntIdMateria()+" and id= "+objeto.getIntIdDocente()+"";
        }
        System.out.println(resultado);
        return resultado;
    }
    
    public String getSQLToModificarClase(clase objeto){
        if(objeto != null){
            resultado = "update tblclase set id = "+objeto.getIntIdDocente()+", "
            + "id_paralelo = "+objeto.getIntIdParalelo()+", id_materia = "+objeto.getIntIdMateria()+" "
            + "where idclase = "+objeto.getIntIdClase()+""; 
        }
        System.out.println(resultado);
        return resultado;
    }
    
//BUSCAR REGISTRO EN LA CLASE CON MATERIA NULLA---------------------------------
    public String getSQLTOBuscarClaseMateriaNula(clase objeto){
        if(objeto != null){
            resultado = "select * from tblclase as tc where tc.id_materia isnull and "
            + "tc.idclase = "+objeto.getIntIdClase()+" and tc.id_paralelo = "+objeto.getIntIdParalelo()+" "
            + "and tc.id = "+objeto.getIntIdDocente()+"";
        }
        System.out.println(resultado);
        return resultado;
    }
    
//ACCESO A DATOS PARA BUSCAR LA ÚLTIMA CLASE INGRESADA--------------------------
    public String getSQLToBuscarUltimaClase(){
        resultado = "select tc.idclase from tblclase as tc order by tc.idclase desc limit 1";
        System.out.println(resultado);
        return resultado;
    }
}

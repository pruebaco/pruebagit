/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ggm.accesodatos;

import com.google.gson.Gson;
import ggm.comun.actividad;
import ggm.comun.clase;
import ggm.comun.estudiante;
import ggm.comun.paralelo;
import ggm.comun.parcial;
import ggm.comun.representante;
import ggm.conexion.AccesoDatos;

/**
 *
 * @author Cristhian
 */
public class estudianteAD {
    private estudiante oEstudiante;
    private representante oRepresentante;
    private Gson oGson;
    private AccesoDatos ac;
    private String resultado;
    
    public estudianteAD(){
        oEstudiante = new estudiante();
        oRepresentante = new representante();
        oGson = new Gson();
        ac = new AccesoDatos();
        resultado = "";
    }
   
//INGRESO DE UN NUEVO ESTUDIANTE------------------------------------------------
    public String getSQLToRegistrarEstudiante(estudiante objeto) {
        if(objeto != null){
            resultado = "INSERT INTO tblestudiante(\n"
                    + "nombre_est, apellido_est, cedula_est)\n"
                    + "VALUES ('"+objeto.getStrNombres()+"', '"+objeto.getStrApellidos()+"','"+objeto.getStrCedula()+"');";
        }
        System.out.println(resultado);
        return resultado;
    }

//MÉTODO PARA EL INGRESO DE ESTUDIANTES DE UN PARALELO--------------------------
    public String getSQLToInsertEstudiantesParalelo(String json){
        String result = "";
        if(!json.equals("")){
            oEstudiante = oGson.fromJson(json, estudiante.class);
            result = "INSERT INTO tblestudiante (apellido_est, nombre_est) "
                    + "VALUES ('"+oEstudiante.getStrApellidos()+"', '"+oEstudiante.getStrNombres()+"')";
        }
        return "result";
    }
    
//MOSTRAR LA INFORMACIÓN DE UN ESTUDIANTE---------------------------------------
    public String getSQLToBuscarEstudiante(estudiante objeto) {
        if(objeto != null){
            resultado = "select * from tblestudiante where cedula_est = '"+objeto.getStrCedula()+"'";
        }
        System.out.println(resultado);
        return resultado;
    }

//MÉTODO PARA EL LISTADO DE ESTUDIANTES POR REPRESENTANTE-----------------------
    public String getSQLToListarEstudiantesDeRepresentante(representante objeto) {
        if( objeto != null){
            resultado = "SELECT te.id_estudiante, te.id, te.nombre_est, te.apellido_est, te.cedula_est, te.edad_est, te.telefonoreferencia\n"
                    +"FROM tblrepresentante as tr inner join tblestudiante as te ON tr.id = te.id\n"
                    +"WHERE tr.cedula = '"+objeto.getStrCedula()+"';";
        }
        System.out.println(resultado);
        return resultado;
    }
    
//ACCESO A DATOS PARA EL LISTADO DE ESTUDIANTES DE UNA CLASE CON NOTAS CUANTITATIVAS
    public String getSQLToListarEstudiantesDeClase(clase objeto, parcial objetoPa) {
        if(objeto != null && objetoPa != null){
//            resultado = "select te.id_estudiante, te.id, te.nombre_est, te.apellido_est, te.cedula_est, tm.idmatricula, "
//                + "tev.idnota, tev.idparcial, tev.idcursa, tev.nota1, tev.nota2, tev.nota3, tev.nota4 from tblcursa as tcu "
//                + "inner join tblmatricula as tm on tcu.idmatricula = tm.idmatricula inner join tblestudiante as te on "
//                + "tm.id_estudiante = te.id_estudiante inner join tblevaluacion as tev on tcu.idcursa = tev.idcursa "
//                + "where tcu.idclase = "+objeto.getIntIdClase()+" and tev.idparcial = "+objetoPa.getIntIdParcial()+"";
                resultado = "select te.id_estudiante, te.id, te.nombre_est, te.apellido_est, te.cedula_est, tm.idmatricula, tev2.idnota, tev2.idparcial, tev2.idcursa, tev2.nota1, tev2.nota2, tev2.nota3, tev2.nota4 "
                    + "from tblcursa as tcu inner join tblmatricula as tm on tcu.idmatricula = tm.idmatricula "
                    + "inner join tblestudiante as te on tm.id_estudiante = te.id_estudiante left join "
                    + "(select * from tblevaluacion as tev where tev.idparcial = "+objetoPa.getIntIdParcial()+" ) as tev2 on tcu.idcursa = tev2.idcursa "
                    + "where tcu.idclase = "+objeto.getIntIdClase()+"";
        }
        System.out.println(resultado);
        return resultado;
    }
    
//MÉTODO PARA MODIFICAR INFORMACIÓN DE UN ESTUDIANTE----------------------------
    public String getSQLToModificarEstudiante(estudiante objeto) {
        if(objeto != null){
            resultado = "UPDATE tblestudiante set nombre_est='"+objeto.getStrNombres()+"', "
                    + "apellido_est='"+objeto.getStrApellidos()+"', edad_est= "+objeto.getIntEdad()+", "
                    + "telefonoreferencia='"+objeto.getStrTelefonoReferencia()+"' "
                    + "WHERE (cedula_est='"+objeto.getStrCedula()+"');";
        }
        System.out.println(resultado);
        return resultado;
    }

//ACCESO A DATOS PARA REGISTRAR NUEVA MATRICULA DE ESTUDIANTE-------------------    
    public String getSQLToRegistrarMatricula(int idEstudiante, paralelo objeto) {
        if(idEstudiante != 0 && objeto != null){
            resultado = "insert into tblmatricula (id_estudiante, id_periodo)"
                    + "VALUES ("+idEstudiante+", "+objeto.getIntIdPeriodo()+")";
        }
        System.out.println(resultado);
        return resultado;
    }

//ACCESO A DATOS PARA RECUPERAR EL ID DE LA ULTIMA MATRICULA DE UN ESTUDIANTE---
    public String getSQLToBuscarIdUltimaMatriculaEstudiante(int idEstudiante) {
        if(idEstudiante != 0){
            resultado = "select idmatricula from tblmatricula where id_estudiante = "
                    + ""+idEstudiante+" ORDER BY idmatricula DESC LIMIT 1";
        }
        System.out.println(resultado);
        return resultado;
    }
    
//ACCESO A DATOS PARA EL REGISTRO DE UN ESTUDIANTE EN UN CURSO------------------
    public String getSQLToRegistrarCursaEstudiante(int idMatricula, paralelo objeto){
        if(idMatricula != 0 && objeto != null){
            resultado = "insert into tblcursa (idmatricula, idclase) values "
                    + "("+idMatricula+", (select tc.idclase from tblparalelo as tp "
                    + "inner join tblclase as tc on tp.id_paralelo = tc.id_paralelo "
                    + "where tp.id_paralelo = "+objeto.getIntIdParalelo()+"))";
        }
        System.out.println(resultado);
        return resultado;
    }

//ACCESO A DATOS PARA RETORNAR EL ID DE UN ESTUDIANTE MEDIANTE SU CEDULA--------    
    public String getSQLToBuscarIdPorCedula(estudiante objeto) {
        if(objeto != null){
            resultado = "select id_estudiante from tblestudiante where cedula_est = '"+objeto.getStrCedula()+"'";
        }
        System.out.println(resultado);
        return resultado;
    }

//ACCESO A DATOS PARA REGISTRAR UN ESTUDIANTE-----------------------------------    
    public String getSQLToInsertEstudianteDatos(estudiante objeto) {
        if(objeto != null){
            resultado = "insert into tblestudiante (cedula_est, nombre_est, apellido_est) "
                    + "values ('"+objeto.getStrCedula()+"', '"+objeto.getStrApellidos()+"', '"+objeto.getStrNombres()+"')";
        }
        System.out.println(resultado);
        return resultado;
    }

//MÉTODO PARA BUSCAR EL REPRESENTANTE DE UN ESTUDIANTE--------------------------
    public String getSQLToBuscarRepresentanteDeEstudiante(estudiante objeto) {
        if(objeto != null){
            resultado = "select * from tblestudiante as te inner join tblrepresentante as tr "
                    + "on te.id = tr.id where te.id_estudiante = "+objeto.getIntIdEstudiante()+"";
            System.out.println(resultado);
        }
        return resultado;
    }

//ACCESO A DATOS PARA LISTAR LOS ESTUDIANTES DE UN PARALELO---------------------
    public String getSQLToListarEstudiantesDeParalelo(paralelo objeto) {
        if(objeto != null){ 
            resultado = "select te.id_estudiante, te.id, te.nombre_est, te.apellido_est, tm.idmatricula, "
            + "te.cedula_est from tblclase as tc inner join tblcursa as tcu on tc.idclase = tcu.idclase "
            + "inner join tblmatricula as tm on tcu.idmatricula = tm.idmatricula inner join tblestudiante as te "
            + "on tm.id_estudiante = te.id_estudiante where tc.id_paralelo = "+objeto.getIntIdParalelo()+" group by te.id_estudiante, tm.idmatricula";
        }
        System.out.println(resultado);
        return resultado;
    }
    
//ACCESO A DATOS PARA EL LISTADO DE ESTUDIANTES SIN REPRESENTANTE DE UN PARALELO
    public String getSQLToListarEstudiantesSinRepresentante(paralelo objeto) {
        if(objeto != null){ 
            resultado = "select te.id_estudiante, te.nombre_est, te.apellido_est, "
            + "te.cedula_est from tblclase as tc inner join tblcursa as tcu on tc.idclase = tcu.idclase "
            + "inner join tblmatricula as tm on tcu.idmatricula = tm.idmatricula inner join tblestudiante as te "
            + "on tm.id_estudiante = te.id_estudiante where tc.id_paralelo = "+objeto.getIntIdParalelo()+"  and te.id isnull";
        }
        System.out.println(resultado);
        return resultado;
    }

//ACCESO A DATOS PARA ASIGNAR EL REPRESENTANTE A UN ESTUDIANTE--------------------------
    public String getSQLToAsignarRepresentanteEstudiante(representante objetoRepresentante, estudiante objeto) {
        if(objeto != null){
            resultado = "UPDATE tblestudiante set id = "+objetoRepresentante.getIntIdUsuario()+" "
            + "WHERE (cedula_est = '"+objeto.getStrCedula()+"')";
        }
        System.out.println(resultado);
        return resultado;
    }    
   
//ACCESO A DATOS PARA LISTAR ESTUDIANTES DE UNA CLASE CON UNA ACTIVIDAD Y SUS NOTAS
    public String getSQLToListarEstudiantesClaseActividadNotas( actividad objetoActividad){
        if(objetoActividad!=null){
            
            resultado = "SELECT te.nombre_est, te.apellido_est, te.cedula_est, ta.nombre_act, "
            + "ea.calificacion from tblestudiante as te inner join estudianteactividad as ea on "
            + "te.id_estudiante = ea.id_estudiante inner join tblactividad as ta "
            + "on ea.id_actividad = ta.id_actividad inner join tblclase as tc on ta.idclase = ta.idclase "
            + "where tc.idclase = "+objetoActividad.getIntIdClase()+" and ta.id_actividad = "+objetoActividad.getIntIdActividad()+"";
            

            
//            resultado = "SELECT te.nombre_est, te.apellido_est, te.cedula_est, ta.nombre_act, "
//            + "ea.calificacion from tblestudiante as te inner join estudianteactividad as ea on "
//            + "te.id_estudiante = ea.id_estudiante inner join tblactividad as ta on ea.id_actividad = ta.id_actividad "
//            + "inner join tblclase as tc on ta.idclase = ta.idclase "
//            + "where tc.id_paralelo = "+objetoClase.getIntIdParalelo()+" and tc.id_materia = "+objetoClase.getIntIdMateria()+" "
//            + "and tc.id = "+objetoClase.getIntIdDocente()+" and ta.id_actividad = "+objetoActividad.getIntIdActividad()+"";
        }
        System.out.println(resultado);
        return resultado;
    }
    
}

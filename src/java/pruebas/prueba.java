/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebas;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import ggm.comun.actividad;
import ggm.comun.materia;
import ggm.comun.paralelo;
import java.util.ArrayList;
import java.util.List;
//import org.json.simple.JSONArray;
//import org.json.simple.JSONObject;

import org.json.JSONArray;
import org.json.JSONObject;


/**
 *
 * @author Cristhian
 */
public class prueba {
    
    public static void main(String[] args) {
//        String strMain = "0602, Pere, Juan, ;";
//        List<String> lista = mandaLista(strMain);
//        System.out.println("Valor del objeto: "+lista);
//        for(int i = 0; i < lista.size(); i++){
//            if(lista.get(i).equals(";")){
//                imprimir(lista.get(i-3), lista.get(i-2), lista.get(i-1));
//            }
//        }
//    }
//    
//    public static List<String> mandaLista(String strMain){
//        String[] arrSplit = strMain.split("[, ]");
//        System.out.println("Valor del array: "+arrSplit.length);
//        List<String> lista = new ArrayList<>();
//        for (int i = 0; i < arrSplit.length; i++){ 
//            if(!arrSplit[i].equals("")){
//                lista.add(arrSplit[i]);
//            }
//        }
//        return lista;
//    }
//    
//    public static void imprimir(String ced, String ape, String nom){
//        String cedula = ced;
//        String apellido = ape;
//        String nombre = nom;
//        System.out.println("Cedula: "+cedula+", Apellido: "+apellido+", Nombre: "+nombre);

//            Gson gson = new Gson();
//            materia obj1 = new materia();
//            obj1.setStrNombreMAteria("Ciencias");
//            obj1.setStrDescripcionMateria("Buena");
//            paralelo obj2 = new paralelo();
//            obj2.setStrParalelo("5to paralelo B");
//
//            JsonObject object = new JsonObject();
//            object.addProperty("materia", gson.toJson(obj1));
//            object.addProperty("paralelo", gson.toJson(obj2));
//            String aux = object.toString();
//            System.out.println("Json formado "+aux);
//            
//            JsonObject jsonObject = new JsonParser().parse(aux).getAsJsonObject();
//            materia obj3 = gson.fromJson(jsonObject.get("materia").getAsString(), materia.class);
//            System.out.println("Materia nueva "+obj3.getStrNombreMAteria());
//            paralelo obj4 = gson.fromJson(jsonObject.get("paralelo").getAsString(), paralelo.class);
//            System.out.println("Paralelo nuevo "+obj4.getStrParalelo());




            actividad objetoActividad = new actividad();
            Gson objetoGson = new Gson();
            JSONArray lista = new JSONArray();
             
            for(int i = 0; i<2 ; i++){
                JSONObject data = new JSONObject();
                data.put("estudiante", "Juan");
                    objetoActividad.setIntIdActividad(1);
                    objetoActividad.setIntIdClase(1);
                    objetoActividad.setStrNombre("nombre");
                    objetoActividad.setStrFechaRealizado("f_realizado");
                    objetoActividad.setStrFechaPlazo("f_plazo");
                    objetoActividad.setStrDescripcion("descripcion_act");
                    objetoActividad.setStrTipo("tipo_act");
                    String auxActividad = objetoGson.toJson(objetoActividad);
                    data.put("actividad", auxActividad);
                    
                    data.put("calificacion", i);
                    lista.put(data);
                
            }
            String FiLista = lista.toString();
            System.out.println("Listado: "+FiLista);
            
            JSONArray array = new JSONArray(FiLista);
            for(int i = 0; i<array.length() ; i++){
                JSONObject obj1 = array.getJSONObject(i);
                String nombre = obj1.getString("estudiante");
                System.out.println("NOMBRE: "+nombre);
                String actividad = obj1.getString("actividad");
                actividad obj = objetoGson.fromJson(actividad, actividad.class);
                System.out.println("ACTIVIDAD: "+obj.getStrDescripcion());
                Integer calificacion = obj1.getInt("calificacion");
                System.out.println("CALIFICACION: "+calificacion);
            }

//            Gson objGson = new Gson();
//            String prueba = "Estamos aqui";
//            String strDescripcion = "{\"strDescripcion\":\""+prueba+"\"}";
//            actividad obj = objGson.fromJson(strDescripcion, actividad.class);
//            System.out.println(">:v: "+obj.getStrDescripcion());

    }
    
}

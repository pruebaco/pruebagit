/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serviciosweb;

import ggm.logicanegocio.sesionLN;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author Cristhian
 */
@WebService(serviceName = "sesionWS")
public class sesionWS {
    private sesionLN SesionLN;
    private subirImagen oImagen;
    
    
    public sesionWS(){
        SesionLN = new sesionLN();
        oImagen = new subirImagen();
    }
    
    /**
     * This is a sample web service operation
     */
    
//MÉTODO PARA EL LOGEO DE USUARIOS---------------------------------------------
    @WebMethod(operationName = "verificarCuenta")
    public String verificarCuenta(@WebParam(name = "json") String json) {
        return SesionLN.loginUsuarios(json);
    }

//MÉTODO PARA EL REGISTRO DE USUARIOS-------------------------------------------    
    @WebMethod(operationName = "registroUsuario")
    public String registroUsuario(
            @WebParam(name = "json") String json,
            @WebParam(name = "periodo") String periodo
    ) {
        return SesionLN.registroUsuario(json, periodo);
    }
    
//MÉTODO PARA LA SUBIDA DE IMAGENES---------------------------------------------    
    @WebMethod(operationName = "subirImagen")
    public String subirImagen(
            @WebParam(name = "bytImagen") byte[] bytImagen,
            @WebParam(name = "cedula") String cedula) {
        String result = oImagen.subirFoto(bytImagen, cedula);
        return result;
    }
    
//MÉTODO PARA SUBIR O ACTUALIZAR TOKENS-----------------------------------------
    @WebMethod(operationName = "subirToken")
    public String subirToken(@WebParam(name = "json") String json){
        return SesionLN.subirToken(json);
    }
}

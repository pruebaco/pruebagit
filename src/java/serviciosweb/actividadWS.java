/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serviciosweb;

import ggm.logicanegocio.actividadLN;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author Cristhian
 */
@WebService(serviceName = "actividadWS")
public class actividadWS {
    private actividadLN oActividadLN;
    
    public actividadWS(){
        oActividadLN = new actividadLN();
    }
    
    /*AUN FALTA EL MÉTODO DE CREACIÓN*/
    
//MÉTODO PARA LA CREACIÓN DE UNA ACTIVIDAD POR PARTE DEL DOCENTE----------------
    @WebMethod(operationName = "ingresarActividad")
    public String ingresarActividad(
            @WebParam(name = "jsonClase")String jsonClase,
            @WebParam(name = "jsonActividad")String jsonActividad,
            @WebParam(name = "lista")String lista){
        return oActividadLN.ingresarActividad(jsonClase, jsonActividad, lista);
    }
    
//MÉTODO PARA LA MODIFICACIÓN DE UNA ACTIVIDAD----------------------------------    
    @WebMethod(operationName = "modificarActividad")
    public String modificarActividad(@WebParam(name = "json") String json) {
        return oActividadLN.modificarActividad(json);
    }
    
//MÉTODO PARA LA RESPUESTA DE UNA ACTIVIDAD-------------------------------------    
    @WebMethod(operationName = "retroalimentacionActividad")
    public String retroalimentacionActividad
        (@WebParam(name = "jsonNotificacion") String jsonNotificacion,
        @WebParam(name = "jsonActividad") String jsonActividad,
        @WebParam(name = "jsonDocente") String jsonDocente,
        @WebParam(name = "jsonRepresentante") String jsonRepresentante,
        @WebParam(name = "jsonParalelo") String jsonParalelo,
        @WebParam(name = "jsonMateria") String jsonMateria,
        @WebParam(name = "jsonEstudiante") String jsonEstudiante,
        @WebParam(name = "jsonCalificacion") String jsonCalificacion
        ){
            System.out.println(jsonNotificacion +" "+ jsonActividad +" "+ jsonDocente +" "+ jsonRepresentante +" "+ jsonParalelo +" "+ jsonMateria +" "+ jsonEstudiante +" "+ jsonCalificacion);
          //return "positivo";  
        return oActividadLN.responderActividad(jsonNotificacion, jsonActividad, jsonDocente, jsonRepresentante, jsonParalelo, jsonMateria, jsonEstudiante, jsonCalificacion);
    }
        
//MÉTODO PARA LISTAR LAS ACTIVIDADES DE LOS REPRESENTADOS DE UN PADRE DE FAMILIA
    //(PENDIENTE PARA SER USADA)
//   @WebMethod(operationName = "actividadesEstudiantesDeRepresentante")
//    public String actividadesEstudiantesDeRepresentante(@WebParam(name = "jsonRepresentante") String jsonRepresentante) {
//        return oActividadLN.actividadesEstudiantesDeRepresentante(jsonRepresentante);
//    }    
        
        
////MÉTODO PARA LISTAR LAS ACTIVIDADES DE UNA CLASE-------------------------------
//    @WebMethod(operationName = "listarActividadesClase")
//    public String listarActividadesClase(@WebParam(name = "jsonClase") String jsonClase) {
//        return oActividadLN.listarActividadesClase(jsonClase);
//    } 
}

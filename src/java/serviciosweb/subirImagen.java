/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serviciosweb;

import java.io.File;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Cristhian
 */
public class subirImagen {
    public subirImagen(){
    }
    
    String subirFoto(byte[] imagen, String cedula){
        try{
            //Especifica las características del archivo a subirse--------------
            DiskFileItemFactory factory = new DiskFileItemFactory();
            factory.setSizeThreshold(2048);
            factory.setRepository(new File(System.getProperty("java.io.tmpdir"))); 
            ServletFileUpload upload = new ServletFileUpload(factory);
            upload.setFileSizeMax(5000000);
            upload.setSizeMax(5000000);
            //Especifica la dirrección y nombre de como será almacenado---------
            String uploadPath = "D:\\Documentos\\NetBeans\\Tesis\\AppGgmGestion\\build\\web\\files";
            File uploadDir = new File(uploadPath);
            if (!uploadDir.exists()) {
                uploadDir.mkdir();
            }
            String fileName = ""+cedula+".jpg";
            String filePath = uploadPath + File.separator + fileName;
            //Convierte el array en un archivo, nuestro caso en jpg-------------
            FileUtils.writeByteArrayToFile(new File(filePath), imagen);
            System.out.println("Subió: "+cedula);
            return "Imagen subida correctamente";
        }catch(Exception e){
            return "Ocurrió algún problema, inténtelo más tarde";
        }
    }
}

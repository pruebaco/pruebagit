/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serviciosweb;

import ggm.logicanegocio.materiaLN;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author Cristhian
 */
@WebService(serviceName = "materiaWS")
public class materiaWS {

    materiaLN objetoMateriaLN;

    public materiaWS() {
        objetoMateriaLN = new materiaLN();
    }
    
    @WebMethod(operationName = "paralelosDeDocenteSinMateria")
    public String paralelosDeDocenteSinMateria() {
        return objetoMateriaLN.listadoParalelosConClaseDeDocente();
    }
    
    @WebMethod(operationName = "ingresarMateria")
    public String ingresarMateria(@WebParam(name = "jsonClase") String jsonClase,
            @WebParam(name = "jsonMateria") String jsonMateria) {
        return objetoMateriaLN.ingresarMateria(jsonClase, jsonMateria);
    }
    
//MÉTODO WEB PARA LISTAR LAS MATERIAS DADO DOCENTE Y PARALELO-------------------    
    @WebMethod(operationName = "materiasDeDocenteParalelo")
    public String materiasDeDocenteParalelo(@WebParam(name = "jsonClase") String jsonClase) {
        
        return objetoMateriaLN.listarMateriasDocenteParalelo(jsonClase);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serviciosweb;

import ggm.logicanegocio.evaluacionLN;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author Cristhian
 */
@WebService(serviceName = "evaluacionWS")
public class evaluacionWS {
    private evaluacionLN objetoEvaluacionLN;
    
    public evaluacionWS(){
        objetoEvaluacionLN = new evaluacionLN();
    }

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "ingresarEvaluacion")
    public String ingresarEvaluacion(@WebParam(name = "jsonClase") String jsonClase,
            @WebParam(name = "jsonMatricula") String jsonMatricula,
            @WebParam(name = "jsonParcial") String jsonParcial,
            @WebParam(name = "jsonEvaluacion") String jsonEvaluacion) {
        System.out.println("SW");
        return objetoEvaluacionLN.ingresoEvaluacion(jsonClase, jsonMatricula, jsonParcial, jsonEvaluacion);
    }
}

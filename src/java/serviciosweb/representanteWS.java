/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serviciosweb;

import ggm.logicanegocio.representanteLN;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author Cristhian
 */
@WebService(serviceName = "representanteWS")
public class representanteWS {    
    private representanteLN RepresentanteLN;
    
    public representanteWS(){
        RepresentanteLN = new representanteLN();
    }
    
//MÉTODO PARA MOSTRAR LA INFORMACIÓN DE UN REPRESENTANTE-----------------------    
    @WebMethod(operationName = "perfilRepresentante")
    public String perfilRepresentante(@WebParam(name = "json") String json) {
        return RepresentanteLN.perfilRepresentante(json);
    }
    
//MÉTODO PARA MODIFICAR LA INFORMACIÓN DE UN REPRESENTANTE----------------------
    @WebMethod(operationName = "modificarRepresentante")
    public String modificarRepresentante(@WebParam(name = "json") String json) {
        return RepresentanteLN.modificarRepresentante(json);
    } 
    
    //MÉTODO PARA LISTAR NOTIFICACIONES (COMPLETAS) DE UN REPRESENTANTE-------------
    @WebMethod(operationName = "notificacionesCompletasRepresentante")
    public String notificacionesCompletasRepresentante(@WebParam(name = "jsonRepresentante") String jsonRepresentante) {
        String aux = RepresentanteLN.notificacionesCompletasRepresentante(jsonRepresentante);
        return aux;
    }
    
}

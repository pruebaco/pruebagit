/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serviciosweb;

import ggm.logicanegocio.paraleloLN;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author Cristhian
 */
@WebService(serviceName = "paraleloWS")
public class paraleloWS {

    paraleloLN objetoParaleloLN; 
    
    public paraleloWS(){
        objetoParaleloLN = new paraleloLN();
    }
    
    //MÉTODO PARA LISTAR TODOS LOS PARALELOS DE LA INSTITUCIÓN------------------
    @WebMethod(operationName = "listarParalelosGeneral")
    public String listarParalelosGeneral(){
        return objetoParaleloLN.listarParalelosGeneral();
    }
    
    //MÉTODO PARA INGRESAR NUEVO PARALELO---------------------------------------
    @WebMethod(operationName = "ingresarParaleloDocente")
    public String ingresarParaleloDocente(
            @WebParam(name = "jsonDocente") String jsonDocente,
            @WebParam(name = "jsonParalelo") String jsonParalelo){
        return objetoParaleloLN.ingresarParaleloDocente(jsonDocente, jsonParalelo);
    }
    
    //MÉTODO PARA LISTAR TODOS LOS PARALELOS DE LA INSTITUCIÓN------------------
//    @WebMethod(operationName = "listarParalelosGeneral")
//    public String listarParalelosGeneral(){
//        return objetoParaleloLN.listarParalelosGeneral();
//    }
}

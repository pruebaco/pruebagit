/*
Se gestiona todas lo que tiene que ver con las notificaciones, desde la creación
actividades por parte del docente y del representante, listado de las mismas, 
hasta la retroalimentación correspondiente por parte de los receptores.
 */
package serviciosweb;

import ggm.logicanegocio.notificacionLN;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author Cristhian
 */
@WebService(serviceName = "notificacionesWS")
public class notificacionWS {

    /**
     * This is a sample web service operation
     */
    
    private notificacionLN oNotificacionesLN;
    public notificacionWS(){
        oNotificacionesLN = new notificacionLN();
    }
    
//MÉTODO PARA ENVIAR UNA NOTIFICACION-------------------------------------------    
//    @WebMethod(operationName = "enviarNotificacion")
//    public String prueba(@WebParam(name = "json") String strToken) {
//        return oNotificacionesLN.enviarNotificacion(strToken);
//    }
    
//MÉTODO PARA LISTAR LAS NOTIFICACIONES DE UN DOCENTE---------------------------    
    @WebMethod(operationName = "listarNotificacionesDocente")
    public String listarNotificacionesDocente(@WebParam(name = "json") String json) {
        return oNotificacionesLN.listarNotificacionesDocente(json);
    }

//MÉTODO PARA LISTAR LAS NOTIFICACIONES DE UN REPRESENTANTE---------------------
    @WebMethod(operationName = "listarNotificacionesRepresentante")
    public String listarNotificacionesRepresentante(@WebParam(name = "json") String json){
        return oNotificacionesLN.listarNotificacionesRepresentante(json);
    }
    
//MÉTODO PARA LA CREACIÓN DE UNA ACTIVIDAD, DOCENTE-----------------------------
//    @WebMethod(operationName = "ingresarActividadDocente")
//    public String ingresarActividadesDocente(
//            @WebParam(name = "json")String json,
//            @WebParam(name = "lista")String lista){
//        //return oNotificacionesLN.ingresarActividadDocente(json, lista);
//        return "El método se encuentra en el modulo de actividades";
//    }
    
//MÉTODO PARA LISTAR NOTIFICACIONES (COMPLETAS) DE UN REPRESENTANTE-------------
//    @WebMethod(operationName = "notificacionesCompletasRepresentante")
//    public String notificacionesCompletasRepresentante(@WebParam(name = "jsonRepresentante") String jsonRepresentante) {
//        return oNotificacionesLN.notificacionesCompletasRepresentante(jsonRepresentante);
//    }
    
    
//MÉTODO PARA LA CREACIÓN DE UNA ACTIVIDAD, REPRESENTANTE-----------------------
//MÉTODO PARA LA MODIFICACIÓN DE UNA ACTIVIDAD----------------------------------    
    
}

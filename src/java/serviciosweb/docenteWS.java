/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serviciosweb;

import ggm.logicanegocio.docenteLN;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author Cristhian
 */
@WebService(serviceName = "docenteWS")
public class docenteWS {

    /**
     * This is a sample web service operation
     */
    
    private docenteLN DocenteLN;
    
    public docenteWS(){
        DocenteLN = new docenteLN();
    }
 //MÉTODO PARA MOSTRAR LA INFORMACIÓN DE UN DOCENTE-----------------------------  
    @WebMethod(operationName = "perfilDocente")
    public String perfilDocente(@WebParam(name = "jsonDocente") String jsonDocente) {
        return DocenteLN.perfilDocente(jsonDocente);
    }
    
//MÉTODO PARA MODIFICAR LA INFORMACIÓN DE UN DOCENTE----------------------------
    @WebMethod(operationName = "modificarDocente")
    public String modificarDocente(@WebParam(name = "json") String json) {
        return DocenteLN.modificarDocente(json);
    }
 
//MÉTODO PARA LISTAR ACTIVIDADES DE UN DOCENTE POR FECHA DE PLAZO---------------
    @WebMethod(operationName = "listarActividadesFechaU")
    public String listarActividadesFechaU(@WebParam(name = "json")String json){
        return DocenteLN.listarActividadesFechaU(json);
    }
    
//MÉTODO PARA LISTAR LOS PARALELOS DE UN DOCENTE--------------------------------
    @WebMethod(operationName = "listarParalelosDocente")
    public String listarParalelos(@WebParam(name = "jsonDocente")String jsonDocente){
        return DocenteLN.listarParalelos(jsonDocente);
    }
    
 //MÉTODO PARA LISTAR LAS MATERIAS DE UN DOCENTE--------------------------------
    @WebMethod(operationName = "actividadesRepresentadosDocente")
    public String actividadesRepresentadosDocente(@WebParam(name = "jsonDocente") String jsonDocente) {
        return DocenteLN.listarMateriasDocente(jsonDocente);
    }
    
//MÉTODO PARA LISTAR PARALELOS CONOCIENDO DOCENTE Y MATERIA---------------------
     @WebMethod(operationName = "listarParalelosDocenteMateria")
    public String listarParalelosMateria(
            @WebParam(name = "jsonClase")String jsonClase){
        return DocenteLN.listarParalelosMateria(jsonClase);
    }    
    
/*
*MÉTODO PARA LISTAR LAS MATERIAS DE UN DOCENTE EN ESPECIFICO--------------------
*/    
    @WebMethod(operationName = "listarMateriasDocente")
    public String listarMateriasDocente(@WebParam(name = "json")String json){
        return DocenteLN.ListarMateriasDocente(json);
    }
    
//LISTAR LAS CLASES DONDE PERTENECE UN DOCENTE, INCLUIDO PARALELO Y MATERIA-----
    @WebMethod(operationName = "ListarClasesDocente")
    public String listarClasesDocente(@WebParam(name = "jsonDocente")String jsonDocente){
        
        return DocenteLN.listarClasesDocente(jsonDocente);
    }
  
//MÉTODO PARA LISTAR TODAS LAS NOTIFICACIONES DE UN DOCENTE---------------------    
@WebMethod(operationName = "notificacionesCompletasDocente")
    public String notificacionesCompletasDocente(@WebParam(name = "jsonDocente") String jsonDocente) {
        String aux = DocenteLN.notificacionesCompletasDocente(jsonDocente);
        return aux;
    }    
    
}

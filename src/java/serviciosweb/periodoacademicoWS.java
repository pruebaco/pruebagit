/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serviciosweb;

import ggm.logicanegocio.actividadLN;
import ggm.logicanegocio.periodoacademicoLN;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author Cristhian
 */
@WebService(serviceName = "periodoacademicoWS")
public class periodoacademicoWS {
    private periodoacademicoLN objetoPeriodo; 
    
    public periodoacademicoWS(){
      objetoPeriodo = new periodoacademicoLN();
    }
    
//SERVICIO PARA EL INGRESO DE UN PERIODO ACADÉMICO------------------------------
    @WebMethod(operationName = "ingresarPeriodoAcademico")
    public String ingresarPeriodoAcademico(@WebParam(name = "json") String json){
        return objetoPeriodo.ingresarPeriodoAcademico(json);
    }
    
//SERVICIO PARA EL LISTADO DE PERIODOS ACADÉMICOS-------------------------------
    @WebMethod(operationName = "listarPeriodoAcademico")
    public String listarPeriodoAcademico(){
        return objetoPeriodo.listarPeriodosAcademicos();
    }
    
    
    actividadLN oActividadLN = new actividadLN();
    //MÉTODO PARA LISTAR LAS ACTIVIDADES DE UNA CLASE-------------------------------
    @WebMethod(operationName = "listarActividadesClase")
    public String listarActividadesClase(@WebParam(name = "jsonClase") String jsonClase) {
        return oActividadLN.listarActividadesClase(jsonClase);
    } 
      
    
}

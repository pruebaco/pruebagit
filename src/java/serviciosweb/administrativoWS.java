/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serviciosweb;


import ggm.logicanegocio.administrativoLN;
import java.io.IOException;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author Cristhian
 */
@WebService(serviceName = "administrativoWS")
public class administrativoWS {

    /**
     * This is a sample web service operation
     */
    
    private administrativoLN AdministrativoLN;
    public administrativoWS(){
        AdministrativoLN = new administrativoLN();
    }
 
//MOSTRAR LA INFORMACION DE UN ADMINISTRATIVO------------------------------------    
    @WebMethod(operationName = "perfilAdministrativo")
    public String perfilAdministrativo(@WebParam(name = "json") String json) {
        return AdministrativoLN.perfilAdminsitrativo(json);
    }
    
//MÉTODO PARA MODIFICAR LA INFORMACIÓN DE UN ADMINISTRATIVO----------------------
    @WebMethod(operationName = "modificarAdministrativo")
    public String modificarAdministrativo(@WebParam(name = "json") String json) {
        String result = AdministrativoLN.modificarAdministrativo(json);
        return result;
    }    
    
  
}

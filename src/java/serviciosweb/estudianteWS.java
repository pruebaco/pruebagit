/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package serviciosweb;

import ggm.logicanegocio.estudianteLN;
import ggm.logicanegocio.evaluacionLN;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author Cristhian
 */
@WebService(serviceName = "estudianteWS")
public class estudianteWS {

    /**
     * This is a sample web service operation
     */
    
    private estudianteLN oEstudianteLN;
    public estudianteWS(){
        oEstudianteLN = new estudianteLN();
    }

//MÉTODO PARA MOSTRAR LA INFORMACIÓN DE UN ESTUDIANTE---------------------------
    @WebMethod(operationName = "perfilEstudiante")
    public String perfilEstudiante(@WebParam(name = "json") String json) {
        return oEstudianteLN.perfilEstudiante(json);
    }
    
//MÉTODO PARA EL REGISTRO DE UN NUEVO ESTUDIANTE---------------------------------
    @WebMethod(operationName = "registrarEstudiante")
    public String registrarEstudiante(@WebParam(name = "json") String json) {
        return oEstudianteLN.registrarEstudiante(json);
    }
    
//MÉTODO PARA INGRESAR LISTA DE ESTUDIANTES DE UN PARALELO----------------------
    @WebMethod(operationName = "ingresoEstudiantesParalelo")
    public String ingresoEstudiantesParalelo(
            @WebParam(name = "jsonParalelo") String jsonParalelo,
            @WebParam(name = "listado") String listado){
        System.out.println("Metodo");
        return oEstudianteLN.ingresoEstudiantesParalelo(jsonParalelo, listado);
    }
    
//MÉTODO PARA MOSTRAR LOS ESTUDIANTES DE UN REPRESENTADO------------------------    
    @WebMethod(operationName = "listadoEstudiantePorRepresentante")
    public String listadoEstudiantePorRepresentante(@WebParam(name = "json") String json) {
        return oEstudianteLN.listadoEstudiantePorRepresentante(json);
    }

//MÉTODO PARA MODIFICAR INFORMACIÓN DE UN ESTUDIANTE----------------------------
    @WebMethod(operationName = "modificarEstudiante")
    public String modificarEstudiante(@WebParam(name = "json") String json) {
        return oEstudianteLN.modificarEstudiante(json);
    }
    
    evaluacionLN objetoEvaluacionLN = new evaluacionLN(); 
    
//MÉTODO PARA LISTAR LOS ESTUDIANTES Y SUS NOTAS DADO UN PARCIAL Y PARALELO-----
    @WebMethod(operationName = "listarEstudiantesParalelo")
    public String listarEstudiantesParalelo(
            @WebParam(name = "jsonClase") String jsonClase,
            @WebParam(name = "jsonParcial") String jsonParcial){
        return objetoEvaluacionLN.listarEstudiantesParalelo(jsonClase, jsonParcial);
    }
  
//MÉTODO PARA LISTAR LOS ESTUDIANTES DE UN  PARALELO---------------------------
    @WebMethod(operationName = "listarEstudiantes")
    public String listarEstudiantes(
            @WebParam(name = "jsonParalelo") String jsonParalelo){
        return oEstudianteLN.listarEstudiantes(jsonParalelo);
    }
    
    
//MÉTODO PARA INGRESAR NOTAS DE UN ESTUDIANTE-----------------------------------
 @WebMethod(operationName = "ingresarEvaluacion")
    public String ingresarEvaluacion(@WebParam(name = "jsonClase") String jsonClase,
            @WebParam(name = "jsonMatricula") String jsonMatricula,
            @WebParam(name = "jsonParcial") String jsonParcial,
            @WebParam(name = "jsonEvaluacion") String jsonEvaluacion) {
        System.out.println("SW");
        return objetoEvaluacionLN.ingresoEvaluacion(jsonClase, jsonMatricula, jsonParcial, jsonEvaluacion);
    }    
    
//MÉTODO PARA LISTAR LOS DOCENTES DE UN ESTUDIANTE------------------------------
    @WebMethod(operationName = "listarDocentesEstudiante")
    public String listarDocentesEstudiante(
            @WebParam(name = "jsonEstudiante") String jsonEstudiante){
        return oEstudianteLN.listarDocentesEstudiante(jsonEstudiante);
    }
    
//MÉTODO PARA LISTAR LOS ESTUDIANTES DE UN PARALELO QUE NO TIENEN ASIGNADO UN REPRESENTANTE 
    @WebMethod(operationName = "listarEstudiantesSinRepresentante")
    public String listarEstudiantesSinRepresentante(
            @WebParam(name = "jsonParalelo") String jsonParalelo){
        return oEstudianteLN.listarEstudiantesSinRepresentante(jsonParalelo);
    }
    
//MÉTODO PARA ASIGNAR UN REPRESENTANTE A UN ESTUDIANTE--------------------------
    @WebMethod(operationName = "asignarRepresentanteEstudiante")
    public String asignarRepresentanteEstudiante(
            @WebParam(name = "jsonRepresentante") String jsonRepresentante,
            @WebParam(name = "jsonEstudiante") String jsonEstudiante){
        return oEstudianteLN.asignarRepresentanteEstudiante(jsonRepresentante, jsonEstudiante);
    }
    
//MÉTODO PARA LISTAR LAS ACTIVIDADES Y NOTAS CUALITATIVAS DE UN ESTUDIANTE------
    @WebMethod(operationName = "listarActividadesNotasEstudiantes")
    public String listarActividadesNotasEstudiantes(
            @WebParam(name = "jsonEstudiante") String jsonEstudiante){
        return oEstudianteLN.listarActividadesNotasEstudiantes(jsonEstudiante);
    }
    
//MÉTODO PARA LISTAR ESTUDIANTES DE UNA CLASE CON LAS NOTAS DE UNA ACTIVIDAD----
   @WebMethod(operationName = "listarEstudiantesClaseActividad")
    public String listarEstudiantesClaseActividad(
           
            @WebParam(name = "jsonActividad") String jsonActividad){
        return oEstudianteLN.listarEstudiantesClaseActividad(jsonActividad);
    } 
}


